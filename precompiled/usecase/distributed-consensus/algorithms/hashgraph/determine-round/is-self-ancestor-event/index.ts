

import { Hashgraph } from '../../../../data-structures/hashgraph'
import { HashgraphEvent } from '../../../../data-structures/hashgraph-event'

function isSelfAncestorEvent (this: Hashgraph, sourceEvent: HashgraphEvent, targetEvent: HashgraphEvent): boolean {
  if (!sourceEvent || !targetEvent) {
    return false
  }
  if (sourceEvent.uid === targetEvent.uid) {
    return true
  }
  if (sourceEvent.creatorUID !== targetEvent.creatorUID) {
    return false
  }
  const sourceEventOrderIndex = this.eventDataTable[sourceEvent.uid].orderIndex
  const targetEventOrderIndex = this.eventDataTable[targetEvent.uid].orderIndex

  return sourceEventOrderIndex > targetEventOrderIndex
}

export {
  isSelfAncestorEvent
}





import { Hashgraph } from '../../../../data-structures/hashgraph'
import { HashgraphEvent } from '../../../../data-structures/hashgraph-event'


function canSeeEventThroughMany (this: Hashgraph, sourceEvent: HashgraphEvent, targetEvent: HashgraphEvent, memberUIDList: string[]): boolean {
  const numVisitedMembers =  this.numberOfVisitedMembersToEvent(sourceEvent, targetEvent, memberUIDList)

  const hasVisitedSupermajority = numVisitedMembers >= Math.ceil((2 / 3) * memberUIDList.length)
  return hasVisitedSupermajority
}

export {
  canSeeEventThroughMany
}



// import { HashgraphShard } from '../../../data-structures/hashgraph-shard'

// function functionName (this: HashgraphShard) {
//     //
// }

// async onAddMemberVote (shard: HashgraphShard, candidateMember: HashgraphShardMember, votingMember: HashgraphShardMember, vote: boolean, roundVoted: number): Promise<void> {

//     // Ensure there is a voting member.
//     if (!votingMember) { return }

//     // Ensure candidate is not already active.
//     if (this.hashgraphUsecase.isActiveMember(shard.hashgraph, candidateMember.publicKey, roundVoted)) { return }

//     // Ensure member has been nominated.
//     if (!shard.addMemberCandidateTable[candidateMember.publicKey]) { return }

//     // Submit the voting member's vote.
//     shard.addMemberCandidateTable[candidateMember.publicKey].voteTable[votingMember.publicKey] = vote

//     const minimumVotePercent: number = shard.addMemberCandidateTable[candidateMember.publicKey].minimumVotePercent
//     const roundNominated: number = shard.addMemberCandidateTable[candidateMember.publicKey].roundNominated

//     const votingMemberList = this.hashgraphUsecase.getMemberList(shard.hashgraph, roundNominated)

//     // Count the number of yes votes.
//     let numOfYesVotes: number = 0

//     for (const member of votingMemberList) {
//       const memberVote = shard.addMemberCandidateTable[candidateMember.publicKey].voteTable[member.uid] ? 1 : 0
//       numOfYesVotes += memberVote
//     }

//     const yesVotePercent: number = Math.floor((numOfYesVotes / votingMemberList.length) * 100)

//     // Ensure there are enough yes votes to add the member.
//     if (yesVotePercent < minimumVotePercent) { return }

//     const selfPublicKey = this.getMemberPublicKey(shard, shard.selfPeerID)

//     let isConnectedPeer = false
//     if (candidateMember.peerConnectionMap) {
//       isConnectedPeer = candidateMember.peerConnectionMap[selfPublicKey]
//     }

//     // Add the member to the shard if they reach minimum number of required votes
//     await this.addMember(shard, candidateMember.peerID!, false, isConnectedPeer, candidateMember, true, roundVoted + 2)
//   }

// export {
//     functionName
// }


import { HashgraphShard } from '../../../../data-structures/hashgraph-shard'

import { sendGossipEventId } from '../add-event-listener-for-send-gossip'

async function sendGossip (this: HashgraphShard, targetPeerID?: string): Promise<void> {

    const selfMember = this.hashgraph.getMember(this.selfPeerID)
    if (!selfMember) {
      return
    }

    const connectedPeerList = Object.keys(selfMember.peerConnectionMap!)
    if (connectedPeerList.length < 1) { return }

    const randomPeerID = targetPeerID || connectedPeerList[Math.floor(Math.random() * connectedPeerList.length)]
    const targetMember = this.hashgraph.getMember(randomPeerID)!

    const latestSelfEvent = this.hashgraph.getMemberLatestEvent(selfMember.uid)
    if (!latestSelfEvent) { return }

    const syncEventList = this.hashgraph.getUnknownEventListForMember(targetMember.uid)
    syncEventList.push(latestSelfEvent)


    if (!this.eventListenerTable[sendGossipEventId]) { return }

    for (let eventListenerId in this.eventListenerTable[sendGossipEventId]) {
      if (!this.eventListenerTable[sendGossipEventId].hasOwnProperty(eventListenerId)) {
        continue
      }

      this.eventListenerTable[sendGossipEventId][eventListenerId](randomPeerID, syncEventList)
    }
  }

export {
  sendGossip
}

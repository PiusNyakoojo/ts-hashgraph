
import { CryptographyProvider } from '../../../provider/service-type/cryptography'
import { MockCryptographyProvider } from '../../../provider/service/mock-cryptography-provider/provider'

import { createEvent } from '../algorithms/hashgraph-member/create-event'
import { verifyEvent } from '../algorithms/hashgraph-member/verify-event'


import * as uuid from 'uuid'


class HashgraphMember {
  uid: string = uuid.v4()

  publicKey?: string = ''
  privateKey?: string = ''

  peerID?: string = ''
  peerConnectionMap?: { [publicKey: string]: boolean } = {}
  details?: any = {}

  cryptographyProvider: CryptographyProvider = new MockCryptographyProvider()

  constructor (memberInfo?: {
    publicKey?: string
    privateKey?: string
    peerID?: string
    peerConnectionMap?: { [publicKey: string]: boolean }
    details?: any
  }, cryptographyProvider?: CryptographyProvider) {
    Object.assign(this, memberInfo)
    this.cryptographyProvider = cryptographyProvider || this.cryptographyProvider
  }

  createEvent: typeof createEvent = createEvent.bind(this)
  verifyEvent: typeof verifyEvent = verifyEvent.bind(this)
}

export {
    HashgraphMember
}



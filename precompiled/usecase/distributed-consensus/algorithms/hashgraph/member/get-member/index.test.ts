
import { expect } from 'chai'

import { Hashgraph } from '../../../../data-structures/hashgraph'
import { HashgraphMember } from '../../../../data-structures/hashgraph-member'

describe('Hashgraph - Get Member', () => {
  it('should get added members', () => {
    const hashgraph = new Hashgraph()
    const member = new HashgraphMember()
    hashgraph.addMember(member)
    const retrievedMember = hashgraph.getMember(member.uid)
    expect(retrievedMember).to.deep.equal(member)
  })
})


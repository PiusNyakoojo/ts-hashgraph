


import { UnknownEventTable } from '../../../../data-structures/hashgraph'
import { HashgraphEvent } from '../../../../data-structures/hashgraph-event'


function addEventToUnknownEventList (unknownEventTable: UnknownEventTable, unknownEventList: HashgraphEvent[], event: HashgraphEvent): void {
    let hasAddedSelfParent = true
    let hasAddedOtherParent = true

    if (unknownEventTable[event.selfParentHash]) {
      hasAddedSelfParent = unknownEventTable[event.selfParentHash].isAddedToList
    }

    if (unknownEventTable[event.otherParentHash]) {
      hasAddedOtherParent = unknownEventTable[event.otherParentHash].isAddedToList
    }

    if (unknownEventTable[event.uid].isAddedToList || !(hasAddedSelfParent && hasAddedOtherParent)) {
      return
    }

    unknownEventList.push(event)
    unknownEventTable[event.uid].isAddedToList = true

    for (const callback of unknownEventTable[event.uid].callbackList) {
      callback()
    }
  }

export {
  addEventToUnknownEventList
}


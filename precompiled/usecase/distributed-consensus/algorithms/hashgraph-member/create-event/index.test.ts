
import { expect } from 'chai'

import { HashgraphMember } from '../../../data-structures/hashgraph-member'

describe('Hashgraph Member - Create Event', () => {
  it('should create event to the hashgraph', async () => {
    const member = new HashgraphMember()
    const event = await member.createEvent()

    expect(event).to.not.be.undefined
  })
})


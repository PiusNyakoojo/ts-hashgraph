
import { Hashgraph } from '../../../../data-structures/hashgraph'
// import { HashgraphMember } from '../../../data-structures/hashgraph-member'
import { HashgraphEvent } from '../../../../data-structures/hashgraph-event'



function getMemberLatestEvent (this: Hashgraph, memberUID: string): HashgraphEvent | null {
  const member = this.memberDataTable[memberUID].member

  if (!member) {
    throw Error(`Hashgraph doesn't contain member with uid: ${memberUID}`)
  }

  const latestEventUid = this.memberDataTable[memberUID].orderedEventUidList[this.memberDataTable[memberUID].orderedEventUidList.length - 1]

  if (!this.eventDataTable[latestEventUid]) {
    return null
  }

  return this.eventDataTable[latestEventUid].event
}


// function getMemberLatestEvent (this: Hashgraph, member: HashgraphMember): HashgraphEvent|null {
//     if (!member) { return null }

//     if (member.orderedEventUidList.length === 0) { return null }

//     const latestEventUID = member.orderedEventUidList[member.orderedEventUidList.length - 1]
//     return this.eventTable[latestEventUID] || null
// }

export {
  getMemberLatestEvent
}


import { Hashgraph } from '../../../../data-structures/hashgraph'
import { HashgraphEvent } from '../../../../data-structures/hashgraph-event'


function determineRound (this: Hashgraph, event: HashgraphEvent): number {

  let round = 1
  const selfParentEvent = this.selfParentEvent(event)
  const otherParentEvent = this.otherParentEvent(event)

  if (selfParentEvent) {
    round = Math.max(round, this.eventDataTable[selfParentEvent.uid].roundCreated)
  }

  if (otherParentEvent) {
    round = Math.max(round, this.eventDataTable[otherParentEvent.uid].roundCreated)
  }

  const numStronglySeenRoundWitnesses = this.stronglySeenRoundWitnesses(event, round).length

  const eventRoundCreated = numStronglySeenRoundWitnesses >= Math.ceil((2 / 3) * this.nPopulationSize) ? round + 1 : round
  this.eventDataTable[event.uid].roundCreated = eventRoundCreated

  let eventIsWitness = (!selfParentEvent && !otherParentEvent)
  if (selfParentEvent) {
    eventIsWitness = eventIsWitness || eventRoundCreated > this.eventDataTable[selfParentEvent.uid].roundCreated
  }

  this.eventDataTable[event.uid].isWitness = eventIsWitness

  if (selfParentEvent) {
    this.eventDataTable[event.uid].isWitness = eventRoundCreated > this.eventDataTable[selfParentEvent.uid].roundCreated
  }

  if (!this.eventRoundCreatedTable[eventRoundCreated]) {
    this.eventRoundCreatedTable[eventRoundCreated] = {
      totalMemberTable: {}, participantMemberTable: {}, eventTable: {}, witnessEventTable: {}, famousEventTable: {}, notFamousEventTable: {}
    }

    this.eventRoundCreatedTable[eventRoundCreated].participantMemberTable[event.creatorUID] = true

    this.eventRoundCreatedTable[eventRoundCreated].totalMemberTable[event.creatorUID] = true

    const previousMemberUidList = Object.keys(this.eventRoundCreatedTable[eventRoundCreated - 1].totalMemberTable)
    for (let i = 0; i < previousMemberUidList.length; i++) {
      const memberUid = previousMemberUidList[i]
      this.eventRoundCreatedTable[eventRoundCreated].totalMemberTable[memberUid] = true
    }
  }

  this.eventRoundCreatedTable[eventRoundCreated].eventTable[event.uid] = true

  if (eventIsWitness) {
    this.eventRoundCreatedTable[eventRoundCreated].witnessEventTable[event.uid] = true
  }

  if (eventRoundCreated > this.latestRoundCreated) {
    this.latestRoundCreated = eventRoundCreated
  }

  return eventRoundCreated
}

export {
  determineRound
}


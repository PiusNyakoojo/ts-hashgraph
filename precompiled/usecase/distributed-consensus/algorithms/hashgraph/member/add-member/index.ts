

import { Hashgraph } from '../../../../data-structures/hashgraph'
import { HashgraphMember } from '../../../../data-structures/hashgraph-member'


function addMember (this: Hashgraph, member: HashgraphMember, roundAdded?: number): boolean {

  if (roundAdded && roundAdded <= 0) {
    throw Error('Must add member in round greater than or equal to 1')
  }

  // Initialize member data table
  if (!this.memberDataTable[member.uid]) {
    this.memberDataTable[member.uid] = {
      member,
      orderedEventUidList: [],
      unorderedEventUidList: []
    }
  }

  const roundToAddMember = roundAdded || this.latestRoundCreated

  // Initialize event round created table
  if (!this.eventRoundCreatedTable[roundToAddMember]) {
    this.eventRoundCreatedTable[roundToAddMember] = {
      totalMemberTable: {},
      participantMemberTable: {},
      eventTable: {},
      witnessEventTable: {},
      famousEventTable: {},
      notFamousEventTable: {}
    }
  }

  this.eventRoundCreatedTable[roundToAddMember].totalMemberTable[member.uid] = true

  if (member.publicKey) {
    this.memberDataToUidMap.publicKeyMap[member.publicKey] = member.uid
  }
  if (member.peerID) {
    this.memberDataToUidMap.peerIdMap[member.peerID] = member.uid
  }

  // Update population size
  if (this.eventRoundCreatedTable[this.latestRoundCreated]) {
    this.nPopulationSize = Object.keys(this.eventRoundCreatedTable[this.latestRoundCreated].totalMemberTable).length
  }
  this.nPopulationSizeTotal = Object.keys(this.memberDataTable).length

  return true
}


export {
  addMember
}


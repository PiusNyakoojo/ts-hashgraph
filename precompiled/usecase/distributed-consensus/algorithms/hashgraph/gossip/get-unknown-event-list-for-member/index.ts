


import { Hashgraph, UnknownEventTable } from '../../../../data-structures/hashgraph'
import { HashgraphEvent } from '../../../../data-structures/hashgraph-event'

import { addEventToUnknownEventList } from '../add-event-to-unknown-event-list'

const MAX_NUM_EVENTS_SEARCHED: number = 100

function getUnknownEventListForMember (this: Hashgraph, memberUID: string): HashgraphEvent[] {
    const eventQueue: string[] = []
    let numEventsSearched: number = 0

    const isVisitedEventTable: { [eventUID: string]: boolean } = {}
    const latestAncestorByMember: { [memberUID: string]: { eventUID: string, orderIndex: number } } = {}
    let numMembersVisited: number = 0

    const latestEvent: HashgraphEvent|null = this.getMemberLatestEvent(memberUID)

    if (latestEvent) {
      latestAncestorByMember[latestEvent.creatorUID] = { eventUID: latestEvent.uid, orderIndex: this.eventDataTable[latestEvent.uid].orderIndex }
      eventQueue.unshift(latestEvent.selfParentHash)
      eventQueue.unshift(latestEvent.otherParentHash)
    }

    // Search for the latest ancestor events for each member
    while (eventQueue.length > 0 && numEventsSearched < MAX_NUM_EVENTS_SEARCHED) {
      const hashgraphEventUID = eventQueue.pop()
      if (!hashgraphEventUID || isVisitedEventTable[hashgraphEventUID]) { continue }

      isVisitedEventTable[hashgraphEventUID] = true
      const hashgraphEvent = this.eventDataTable[hashgraphEventUID].event

      if (!hashgraphEvent) { continue }

      if (latestAncestorByMember[hashgraphEvent.creatorUID]) {
        if (latestAncestorByMember[hashgraphEvent.creatorUID].orderIndex < this.eventDataTable[hashgraphEvent.uid].orderIndex) {
          latestAncestorByMember[hashgraphEvent.creatorUID].eventUID = hashgraphEventUID
          latestAncestorByMember[hashgraphEvent.creatorUID].orderIndex = this.eventDataTable[hashgraphEvent.uid].orderIndex
        }
      } else {
        latestAncestorByMember[hashgraphEvent.creatorUID] = { eventUID: hashgraphEventUID, orderIndex: this.eventDataTable[hashgraphEvent.uid].orderIndex }
        numMembersVisited++
      }

      if (numMembersVisited === this.nPopulationSize) {
        break
      }

      if (hashgraphEvent.selfParentHash) {
        eventQueue.unshift(hashgraphEvent.selfParentHash)
      }

      if (hashgraphEvent.otherParentHash) {
        eventQueue.unshift(hashgraphEvent.otherParentHash)
      }

      numEventsSearched++
    }

    // Record the descendants of the latest ancestor events for each member
    let descendantEventList: HashgraphEvent[] = []
    const unknownEventTable: UnknownEventTable = {}

    for (const memberUID of Object.keys(this.memberDataTable)) {

      let latestAncestorEventOrderIndex = -1
      if (latestAncestorByMember[memberUID]) {
        latestAncestorEventOrderIndex = latestAncestorByMember[memberUID].orderIndex
      }

      const hashgraphMember = this.memberDataTable[memberUID].member

      // Ensure we collect the latest ancestor event from all members. Track the earliest event of a member if the
      // member has no events that are ancestors to the target member's latest event.
      const selfDescendantEventUidList = this.memberDataTable[hashgraphMember.uid].orderedEventUidList.slice(latestAncestorEventOrderIndex + 1) || []

      const selfDescendantEventList = selfDescendantEventUidList.map((uid) => this.eventDataTable[uid].event).filter((event) => event ? true : false)

      for (const event of selfDescendantEventList) {
        unknownEventTable[event.uid] = { isAddedToList: false, callbackList: [] }
      }

      descendantEventList = descendantEventList.concat(selfDescendantEventList)
    }

    // Sort the descendant events by the earliest parents first (topological order)

    const unknownEventList: HashgraphEvent[] = []

    for (const event of descendantEventList) {
      let hasAddedSelfParent = true
      let hasAddedOtherParent = true

      if (unknownEventTable[event.selfParentHash]) {
        hasAddedSelfParent = unknownEventTable[event.selfParentHash].isAddedToList
      }

      if (unknownEventTable[event.otherParentHash]) {
        hasAddedOtherParent = unknownEventTable[event.otherParentHash].isAddedToList
      }

      if (!(hasAddedSelfParent && hasAddedOtherParent)) {
        if (!hasAddedSelfParent) {
          unknownEventTable[event.selfParentHash].callbackList.push(() => { addEventToUnknownEventList(unknownEventTable, unknownEventList, event) })
        }

        if (!hasAddedOtherParent) {
          unknownEventTable[event.otherParentHash].callbackList.push(() => { addEventToUnknownEventList(unknownEventTable, unknownEventList, event) })
        }

        continue
      }

      addEventToUnknownEventList(unknownEventTable, unknownEventList, event)
    }

    return unknownEventList
}

export {
  getUnknownEventListForMember
}


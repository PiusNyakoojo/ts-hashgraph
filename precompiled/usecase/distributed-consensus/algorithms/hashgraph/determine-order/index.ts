
import { Hashgraph } from '../../../data-structures/hashgraph'
import { HashgraphEvent } from '../../../data-structures/hashgraph-event'

import { middleBitOfSignature } from '../../../../../miscellaneous/bit-operations/algorithms/middle-bit-of-signature'
import { stringToBinary } from '../../../../../miscellaneous/bit-operations/algorithms/string-to-binary'


function determineOrder (this: Hashgraph, earliestRoundReceived?: number): HashgraphEvent[] {
  const roundList = Object.keys(this.eventRoundCreatedTable).map((round: string) => parseInt(round, 10))
                    .filter((round: number) => (round > this.earliestRoundCreatedWithKnownConsensus)).sort((a, b) => a - b)
  const roundStartIndex = roundList[0]
  const roundEndIndex = roundList[roundList.length - 1]

  for (let i = roundStartIndex; i < roundEndIndex; i++) {
    if (!this.eventRoundCreatedTable[i]) { continue }

    const eventList = Object.keys(this.eventRoundCreatedTable[i].eventTable).map((eventUID) => this.eventDataTable[eventUID].event)

    for (let j = i + 1; j < roundEndIndex; j++) {
      if (!this.eventRoundCreatedTable[j]) { continue }

      if (!this.eventRoundReceivedTable[j]) {
        this.eventRoundReceivedTable[j] = { eventTable: {} }
      }

      const laterWitnessEventList = Object.keys(this.eventRoundCreatedTable[j].witnessEventTable).map((eventUID) => this.eventDataTable[eventUID].event)

      // Ensure that fame has been decided for witness events in round j.
      let isFameDecidedForAll = true
      for (const laterWitnessEvent of laterWitnessEventList) {
        if (!this.eventDataTable[laterWitnessEvent.uid].isFameDecided) {
          isFameDecidedForAll = false
          break
        }
      }

      if (!isFameDecidedForAll) {
        continue // TODO: should this be a break? It would assume that later rounds don't have witnesses where fame has been decided for all.
      }

      const famousEventList = Object.keys(this.eventRoundCreatedTable[j].famousEventTable).map((eventUID) => this.eventDataTable[eventUID].event)

      let numOfEventsWithKnownConsensus = 0

      for (const eventX of eventList) {
        if (this.eventDataTable[eventX.uid].consensusTimestamp) {
          numOfEventsWithKnownConsensus++
          continue
        }

        // Ensure all famous events in round j can see eventX.
        let isSeenByAllFamousEvents = true
        for (const famousEvent of famousEventList) {
          const canSeeEvent = this.canSeeEvent(famousEvent, eventX)
          if (!canSeeEvent) {
            isSeenByAllFamousEvents = false
            break
          }
        }

        if (!isSeenByAllFamousEvents) {
          continue
        }

        if (!this.eventRoundReceivedTable[j].eventTable[eventX.uid]) {
          this.eventDataTable[eventX.uid].roundReceived = j
          this.eventRoundReceivedTable[j].eventTable[eventX.uid] = { medianTimestamp: '', timestampList: [] }
        }

        // Collect the timestamps from the earliest events that see eventX from each member.
        let timestampList: string[] = []

        for (const famousEvent of famousEventList) {
          let earliestEventOrderIndex: number = this.eventDataTable[famousEvent.uid].orderIndex

          for (let k = earliestEventOrderIndex; k >= 0; k--) {
            const earlierEventUID = this.memberDataTable[famousEvent.creatorUID].orderedEventUidList[k]
            const earlierEvent = this.eventDataTable[earlierEventUID].event

            const canSeeEventX = this.canSeeEvent(earlierEvent, eventX)

            earliestEventOrderIndex = canSeeEventX ? k : k + 1
            if (!canSeeEventX) { break }
          }

          const earliestEventUID = this.memberDataTable[famousEvent.creatorUID].orderedEventUidList[earliestEventOrderIndex]
          const earliestEventToSeeEventX = this.eventDataTable[earliestEventUID].event

          timestampList.push(earliestEventToSeeEventX.timestamp)
        }

        timestampList = timestampList.sort((a, b) => parseInt(a, 10) - parseInt(b, 10))

        const middleIndex = Math.floor(famousEventList.length / 2)
        const medianTimestamp = timestampList[middleIndex]

        this.eventRoundReceivedTable[j].eventTable[eventX.uid].medianTimestamp = medianTimestamp
        this.eventRoundReceivedTable[j].eventTable[eventX.uid].timestampList = timestampList
        this.eventDataTable[eventX.uid].consensusTimestamp = medianTimestamp

        numOfEventsWithKnownConsensus++
      }

      if (numOfEventsWithKnownConsensus === eventList.length) {
        this.earliestRoundCreatedWithKnownConsensus = i
      }
    }
  }

  let orderedEventList: HashgraphEvent[] = []

  const roundReceivedList = Object.keys(this.eventRoundReceivedTable).map((round: string) => parseInt(round, 10))
                            .filter((round: number) => (round > (earliestRoundReceived || 0))).sort((a, b) => a - b)
  const roundReceivedStartIndex = roundReceivedList[0]
  const roundReceivedEndIndex = roundReceivedList[roundReceivedList.length - 1]

  // Sort events by round received
  for (let i = roundReceivedStartIndex; i < roundReceivedEndIndex; i++) {
    const eventList = Object.keys(this.eventRoundReceivedTable[i].eventTable).map((eventUID) => this.eventDataTable[eventUID].event)
    .sort((eventA: HashgraphEvent, eventB: HashgraphEvent) => (parseInt(this.eventDataTable[eventA.uid].consensusTimestamp, 10) - parseInt(this.eventDataTable[eventB.uid].consensusTimestamp, 10)))

    orderedEventList = orderedEventList.concat(eventList)
  }

  // Sort events by median timestamp, then extended median timestamp, then whitened signature
  orderedEventList = orderedEventList.sort((eventA: HashgraphEvent, eventB: HashgraphEvent) => {
    const timeA = parseInt(this.eventDataTable[eventA.uid].consensusTimestamp, 10)
    const timeB = parseInt(this.eventDataTable[eventB.uid].consensusTimestamp, 10)

    // Sort by median timestamp
    if (timeA !== timeB) {
      return timeA - timeB
    } else {

      // Sort by extended median timestamp
      const timestampListA = this.eventRoundReceivedTable[this.eventDataTable[eventA.uid].roundReceived].eventTable[eventA.uid].timestampList
      const timestampListB = this.eventRoundReceivedTable[this.eventDataTable[eventB.uid].roundReceived].eventTable[eventB.uid].timestampList

      const shorterTimestampList = timestampListA.length <= timestampListB.length ? timestampListA : timestampListB

      const middleIndex = Math.floor(shorterTimestampList.length / 2)

      for (let l = 0; l <= middleIndex; l++) {
        const leftTimestampA = timestampListA[middleIndex - l]
        const leftTimestampB = timestampListB[middleIndex - l]

        if ((leftTimestampA && leftTimestampB) && (leftTimestampA !== leftTimestampB)) {
          return (parseInt(leftTimestampA, 10) - parseInt(leftTimestampB, 10))
        }

        const rightTimestampA = timestampListA[middleIndex + l]
        const rightTimestampB = timestampListB[middleIndex + l]

        if ((rightTimestampA && rightTimestampB) && (rightTimestampA !== rightTimestampB)) {
          return (parseInt(rightTimestampA, 10) - parseInt(rightTimestampB, 10))
        }
      }

      // Sort by whitened signature
      const signatureA = stringToBinary(eventA.signature) ^ middleBitOfSignature(eventA.signature) // tslint:disable-line: no-bitwise
      const signatureB = stringToBinary(eventB.signature) ^ middleBitOfSignature(eventB.signature) // tslint:disable-line: no-bitwise

      return signatureA - signatureB
    }
  })

  return orderedEventList
}

export {
  determineOrder
}


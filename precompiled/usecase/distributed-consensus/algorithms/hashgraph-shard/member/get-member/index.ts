
import { HashgraphShard } from '../../../../data-structures/hashgraph-shard'
import { HashgraphMember } from '../../../../data-structures/hashgraph-member'

function getMember (this: HashgraphShard, peerID: string): HashgraphMember | null {
  return this.hashgraph.getMember(peerID)
}

export {
  getMember
}


import { expect } from 'chai'

import { Hashgraph } from '../../../../data-structures/hashgraph'
import { HashgraphMember } from '../../../../data-structures/hashgraph-member'

describe('Hashgraph - Get Member List', () => {
  it('should get the list of members', async () => {
    const hashgraph = new Hashgraph()
    const member1 = new HashgraphMember()
    const member2 = new HashgraphMember()
    hashgraph.addMember(member1)
    hashgraph.addMember(member2)

    const memberList = hashgraph.getMemberList()

    const isEveryMemberInList = memberList.every((member: HashgraphMember) => [member1.uid, member2.uid].indexOf(member.uid) >= 0)

    expect(isEveryMemberInList).to.equal(true)
    expect(memberList.length).to.equal(2)
  })
})


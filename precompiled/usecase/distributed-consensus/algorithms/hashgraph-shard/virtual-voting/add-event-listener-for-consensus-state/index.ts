
import { HashgraphShard } from '../../../../data-structures/hashgraph-shard'

import * as uuid from 'uuid'

const consensusStateEventId = 'consensus_state'
type consensusStateEventListenerFunctionType = (event: { previousState: any, newState: any }) => Promise<void> | void

function addEventListenerForConsensusState (this: HashgraphShard, eventListenerFunction: consensusStateEventListenerFunctionType): string {
  const eventListenerId = uuid.v4()

  if (!this.eventListenerTable[consensusStateEventId]) {
    this.eventListenerTable[consensusStateEventId] = {}
  }

  this.eventListenerTable[consensusStateEventId][eventListenerId] = eventListenerFunction

  return eventListenerId
}

export {
  addEventListenerForConsensusState,
  consensusStateEventListenerFunctionType,
  consensusStateEventId
}


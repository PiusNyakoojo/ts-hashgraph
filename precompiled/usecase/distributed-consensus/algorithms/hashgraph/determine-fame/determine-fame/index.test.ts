
import { expect } from 'chai'

import { createHashgraphExample } from '../../hashgraph/create-hashgraph/create-hashgraph-example'
import { exampleHashgraph2 } from '../../hashgraph/create-hashgraph/create-hashgraph-example.test'

describe('Hashgraph - Determine Fame', () => {
  it('should determine fame for N events', async () => {
    const hashgraph = await createHashgraphExample(exampleHashgraph2)

    hashgraph.determineFame()

    const numberOfFamousEventsForRound1 = Object.keys(hashgraph.eventRoundCreatedTable[1].famousEventTable).length
    expect(numberOfFamousEventsForRound1).to.equal(4)

    const numberOfFamousEventsForRound2 = Object.keys(hashgraph.eventRoundCreatedTable[2].famousEventTable).length
    const numberOfNotFamousEventsForRound2 = Object.keys(hashgraph.eventRoundCreatedTable[2].notFamousEventTable).length
    expect(numberOfFamousEventsForRound2).to.equal(3)
    expect(numberOfNotFamousEventsForRound2).to.equal(1)

    const numberOfFamousEventsForRound3 = Object.keys(hashgraph.eventRoundCreatedTable[3].famousEventTable).length
    expect(numberOfFamousEventsForRound3).to.equal(0)

    const numberOfFamousEventsForRound4 = Object.keys(hashgraph.eventRoundCreatedTable[4].famousEventTable).length
    expect(numberOfFamousEventsForRound4).to.equal(0)
  })
})
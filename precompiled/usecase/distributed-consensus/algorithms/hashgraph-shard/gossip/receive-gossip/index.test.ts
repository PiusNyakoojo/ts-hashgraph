
import { expect } from 'chai'

import { HashgraphShard } from '../../../../data-structures/hashgraph-shard'

describe('Hashgraph Shard - Receive Gossip', () => {
  it('should create event to commemorate sync', async () => {

    // Create hashgraph shards
    const shardA = new HashgraphShard()
    shardA.addMember({ peerID: 'alice', isSelfPeer: true })
    const shardB = new HashgraphShard()
    shardA.addMember({ peerID: 'bob', isSelfPeer: true })

    // Exchange peer information
    const aliceMember = shardA.getMember('alice')!
    const bobMember = shardB.getMember('bob')!

    shardA.addMember({ candidateMember: bobMember, skipVoting: true })
    shardB.addMember({ candidateMember: aliceMember, skipVoting: true })

    // Gossip
    shardA.addEventListener('send_gossip', async (event: any) => {
      switch (event.targetPeerID) {
        case 'bob':
          await shardB.receiveGossip(event.eventList)
          break
        default:
          break
      }
    })

    shardB.addEventListener('send_gossip', async (event: any) => {
      switch (event.targetPeerID) {
        case 'alice':
          await shardA.receiveGossip(event.eventList)
          break
        default:
          break
      }
    })

    for (let i = 0; i < 10; i++) {
      shardA.manualSync()
    }

    await new Promise((resolve) => setTimeout(resolve, 1000))

  })
})

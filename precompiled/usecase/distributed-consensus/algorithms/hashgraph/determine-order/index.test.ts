

import { expect } from 'chai'

import { createHashgraphExample } from '../hashgraph/create-hashgraph/create-hashgraph-example'
import { exampleHashgraph2 } from '../hashgraph/create-hashgraph/create-hashgraph-example.test'

describe('Hashgraph - Determine Order', () => {
  it('should determine order for N events', async () => {
    const hashgraph = await createHashgraphExample(exampleHashgraph2)

    hashgraph.determineFame()
    const orderedEventList = hashgraph.determineOrder()

    expect(orderedEventList.length).to.equal(10)

    const sortedOrderedEventUidList = orderedEventList.map((event) => event.uid).sort((a, b) => {
      const consensusTimeStampA = hashgraph.eventDataTable[a].consensusTimestamp
      const consensusTimeStampB = hashgraph.eventDataTable[b].consensusTimestamp
      expect(consensusTimeStampA).to.not.be.undefined
      expect(consensusTimeStampB).to.not.be.undefined
      return (new Date(consensusTimeStampB)).getTime() - (new Date(consensusTimeStampA)).getTime()
    })

    expect(sortedOrderedEventUidList).to.deep.equal(orderedEventList.map((event) => event.uid))
  })
})

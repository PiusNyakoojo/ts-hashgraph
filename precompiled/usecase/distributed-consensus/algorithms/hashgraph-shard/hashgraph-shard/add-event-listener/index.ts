
import { HashgraphShard } from '../../../../data-structures/hashgraph-shard'

import {
  sendGossipEventId,
  sendGossipEventListenerFunctionType
} from '../../gossip/add-event-listener-for-send-gossip'

import {
  transactionOrderEventId,
  transactionOrderEventListenerFunctionType
} from '../../virtual-voting/add-event-listener-for-transaction-order'

import {
  consensusStateEventId,
  consensusStateEventListenerFunctionType
} from '../../virtual-voting/add-event-listener-for-consensus-state'


type eventIdType = typeof sendGossipEventId | typeof transactionOrderEventId | typeof consensusStateEventId
type eventListenerFunctionType = sendGossipEventListenerFunctionType | transactionOrderEventListenerFunctionType | consensusStateEventListenerFunctionType

function addEventListener (this: HashgraphShard, eventId: eventIdType, eventListenerFunction: eventListenerFunctionType): string {
  let eventListenerId = ''

  switch(eventId) {
    case sendGossipEventId:
      eventListenerId = this.addEventListenerForSendGossip(eventListenerFunction as sendGossipEventListenerFunctionType)
      break
    case transactionOrderEventId:
      eventListenerId = this.addEventListenerForTransactionOrder(eventListenerFunction as transactionOrderEventListenerFunctionType)
      break
    case consensusStateEventId:
      eventListenerId = this.addEventListenerForConsensusState(eventListenerFunction as consensusStateEventListenerFunctionType)
      break
  }

  return eventListenerId
}

export {
  addEventListener
}


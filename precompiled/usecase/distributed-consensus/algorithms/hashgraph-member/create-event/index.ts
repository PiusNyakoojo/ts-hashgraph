
import { HashgraphMember } from '../../../data-structures/hashgraph-member'
import { HashgraphEvent } from '../../../data-structures/hashgraph-event'


async function createEvent (this: HashgraphMember, payload?: any, selfParentHash?: string, otherParentHash?: string): Promise<HashgraphEvent> {

  const event = new HashgraphEvent(payload, this.uid, selfParentHash, otherParentHash)

  const eventMessage = {
    payload: event.payload,
    creatorUID: event.creatorUID,
    selfParentHash: event.selfParentHash,
    otherParentHash: event.otherParentHash,
    timestamp: event.timestamp
  }

  // Sign Event
  const signature = await this.cryptographyProvider.signMessage(eventMessage, this.privateKey!)
  event.signature = signature

  // Create Event Hash
  const eventHash = await this.cryptographyProvider.hashObject({
    ...eventMessage, signature
  })
  event.uid = eventHash

  return event
}



// async function addEvent (this: Hashgraph, member: HashgraphMember, event: HashgraphEvent): Promise<boolean> {

//     if (!event) { return false }

//     // Ensure event isn't already added.
//     if (this.eventTable[event.uid]) { return false }

//     // Ensure event is valid
//     const isValidEvent = await event.verifyEvent(member, event)
//     if (!isValidEvent) { return false }

//     // Reset voting properties to default settings.
//     event.orderIndex = -1
//     event.roundCreated = -1
//     event.roundReceived = -1
//     event.consensusTimestamp = ''
//     event.isWitness = false
//     event.isFamous = false
//     event.isFameDecided = false

//     this.eventTable[event.uid] = event

//     const eventOrderIndex = this.findEventOrderIndex(this, event)

//     // Ensure event is added after its self parent
//     if (eventOrderIndex < 0) {
//       const unorderedEventIndex = member.unorderedEventUidList.length
//       member.unorderedEventUidList.push(event.uid)

//       this.onSelfParentOrderFoundListenerMap[event.selfParentHash] = (selfParentOrderIndex: number) => {
//         if (this.eventTable[event.uid].orderIndex >= 0) { return }

//         member.unorderedEventUidList.splice(unorderedEventIndex, 1)
//         this.eventTable[event.uid].orderIndex = selfParentOrderIndex + 1
//         member.orderedEventUidList.splice(selfParentOrderIndex + 1, 0, event.uid)

//         if (this.onSelfParentOrderFoundListenerMap[event.uid]) {
//           this.onSelfParentOrderFoundListenerMap[event.uid](selfParentOrderIndex + 1)
//         }
//       }
//     // Set the order index of the event
//     } else if (eventOrderIndex >= 0) {
//       if (this.eventTable[event.uid].orderIndex >= 0) { return false }

//       this.eventTable[event.uid].orderIndex = eventOrderIndex
//       member.orderedEventUidList.splice(eventOrderIndex, 0, event.uid)

//       if (this.onSelfParentOrderFoundListenerMap[event.uid]) {
//         this.onSelfParentOrderFoundListenerMap[event.uid](eventOrderIndex)
//       }
//     }

//     return true
// }



export {
  createEvent
}




import { expect } from 'chai'

import { Hashgraph } from '../../../../data-structures/hashgraph'
import { HashgraphMember } from '../../../../data-structures/hashgraph-member'

describe('Hashgraph - Add Event', () => {
  it('should have 0 events to start', () => {
    const hashgraph = new Hashgraph()
    const numberOfEvents = Object.keys(hashgraph.eventDataTable).length
    expect(numberOfEvents).to.equal(0)
  })
  it('should add 1 event', async () => {
    const hashgraph = new Hashgraph()
    const member = new HashgraphMember()
    hashgraph.addMember(member)
    const event = await member.createEvent()
    await hashgraph.addEvent(event)
    const numberOfEvents = Object.keys(hashgraph.eventDataTable).length
    expect(numberOfEvents).to.equal(1)
  })
  it('should add N events', async () => {
    const hashgraph = new Hashgraph()
    const member1 = new HashgraphMember()
    const member2 = new HashgraphMember()
    hashgraph.addMember(member1)
    hashgraph.addMember(member2)

    const nNumberOfEvents = 10

    for (let i = 0; i < nNumberOfEvents; i++) {
      let member = i % 2 === 0 ? member1 : member2
      let otherMember = i % 2 === 0 ? member2 : member1

      const selfParentEvent = hashgraph.getMemberLatestEvent(member.uid)
      const otherParentEvent = hashgraph.getMemberLatestEvent(otherMember.uid)

      const selfParentHash = selfParentEvent ? selfParentEvent.uid : ''
      const otherParentHash = otherParentEvent ? otherParentEvent.uid : ''

      const event = await member.createEvent(null, selfParentHash, otherParentHash)
      await hashgraph.addEvent(event)
    }

    const numberOfEvents = Object.keys(hashgraph.eventDataTable).length
    expect(numberOfEvents).to.equal(nNumberOfEvents)
  })
})

import { expect } from 'chai'

import { createHashgraphExample } from '../../hashgraph/create-hashgraph/create-hashgraph-example'
import { exampleHashgraph2 } from '../../hashgraph/create-hashgraph/create-hashgraph-example.test'

describe('Hashgraph - Highest Ancestor Event From Member', () => {
  it('should return highest ancestor event from N Members', async () => {
    const hashgraph = await createHashgraphExample(exampleHashgraph2)

    const memberA = hashgraph.getMember('a')!
    const memberB = hashgraph.getMember('b')!
    const memberC = hashgraph.getMember('c')!
    const memberD = hashgraph.getMember('d')!

    const latestEventFromMemberA = hashgraph.getMemberLatestEvent(memberA.uid)!

    const earliestEventFromMemberB = hashgraph.eventDataTable[hashgraph.memberDataTable[memberB.uid].orderedEventUidList[0]].event
    const earliestEventFromMemberC = hashgraph.eventDataTable[hashgraph.memberDataTable[memberC.uid].orderedEventUidList[0]].event
    const earliestEventFromMemberD = hashgraph.eventDataTable[hashgraph.memberDataTable[memberD.uid].orderedEventUidList[0]].event

    const highestAncestorFromMemberB = hashgraph.highestAncestorEventFromMember(latestEventFromMemberA, earliestEventFromMemberB)!
    const highestAncestorFromMemberC = hashgraph.highestAncestorEventFromMember(latestEventFromMemberA, earliestEventFromMemberC)!
    const highestAncestorFromMemberD = hashgraph.highestAncestorEventFromMember(latestEventFromMemberA, earliestEventFromMemberD)!

    const highestAncestorFromMemberBOrderIndex = hashgraph.eventDataTable[highestAncestorFromMemberB.uid].orderIndex
    const highestAncestorFromMemberCOrderIndex = hashgraph.eventDataTable[highestAncestorFromMemberC.uid].orderIndex
    const highestAncestorFromMemberDOrderIndex = hashgraph.eventDataTable[highestAncestorFromMemberD.uid].orderIndex

    expect(highestAncestorFromMemberBOrderIndex).to.equal(9)
    expect(highestAncestorFromMemberCOrderIndex).to.equal(3)
    expect(highestAncestorFromMemberDOrderIndex).to.equal(8)

  })
})





import { expect } from 'chai'

import { createHashgraphExample } from '../../hashgraph/create-hashgraph/create-hashgraph-example'
import { exampleHashgraph2 } from '../../hashgraph/create-hashgraph/create-hashgraph-example.test'


describe('Hashgraph - Number of Visited Members To Events', () => {
  it('should have many members for N events', async () => {
    const hashgraph = await createHashgraphExample(exampleHashgraph2)

    const memberUidList = hashgraph.getMemberList().map((member) =>  member.uid)
    const numberOfSupermajority = Math.ceil((2 / 3) * memberUidList.length)

    for (let i = 0; i < memberUidList.length; i++) {
      const memberUid = memberUidList[i]
      const sourceEvent  = hashgraph.getMemberLatestEvent(memberUid)!

      for (let j = 0; j < memberUidList.length; j++) {
        const targetMemberUid = memberUidList[j]
        const targetEventUid = hashgraph.memberDataTable[targetMemberUid].orderedEventUidList[0]
        const targetEvent = hashgraph.eventDataTable[targetEventUid].event
        const numberOfVisiitedMembers = hashgraph.numberOfVisitedMembersToEvent(sourceEvent, targetEvent, memberUidList)

        expect(numberOfVisiitedMembers).to.be.greaterThanOrEqual(numberOfSupermajority)
      }
    }
  })
  it('should not have many members for N events', async () => {
    const hashgraph = await createHashgraphExample(exampleHashgraph2)

    const memberUidList = hashgraph.getMemberList().map((member) =>  member.uid)
    const numberOfSupermajority = Math.ceil((2 / 3) * memberUidList.length)

    for (let i = 0; i < memberUidList.length; i++) {
      const memberUid = memberUidList[i]
      const sourceEventUid = hashgraph.memberDataTable[memberUid].orderedEventUidList[0]
      const sourceEvent  = hashgraph.eventDataTable[sourceEventUid].event

      for (let j = 0; j < memberUidList.length; j++) {
        const targetMemberUid = memberUidList[j]
        const targetEventUid = hashgraph.memberDataTable[targetMemberUid].orderedEventUidList[0]
        const targetEvent = hashgraph.eventDataTable[targetEventUid].event
        const numberOfVisiitedMembers = hashgraph.numberOfVisitedMembersToEvent(sourceEvent, targetEvent, memberUidList)

        expect(numberOfVisiitedMembers).to.be.lessThan(numberOfSupermajority)
      }
    }
  })
})


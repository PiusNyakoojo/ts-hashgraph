
// import { HashgraphShard } from '../../../data-structures/hashgraph-shard'

// function functionName (this: HashgraphShard) {
//     //
// }

// async onRemoveMemberNomination (shard: HashgraphShard, candidateMember: HashgraphShardMember, roundNominated: number): Promise<void> {

//     // Ensure candidate is already active.
//     if (!this.hashgraphUsecase.isActiveMember(shard.hashgraph, candidateMember.publicKey)) { return }

//     // Ensure member hasn't already been nominated.
//     if (shard.removeMemberCandidateTable[candidateMember.publicKey]) { return }

//     let minimumVotePercent: number = shard.defaultMinimumVotePercent
//     if (shard.setVotePercentToRemoveMemberCallback) {
//       minimumVotePercent = await shard.setVotePercentToRemoveMemberCallback(candidateMember)
//     }

//     // Add the member as a candidate
//     shard.removeMemberCandidateTable[candidateMember.publicKey] = {
//       candidate: candidateMember, roundNominated, voteTable: {}, minimumVotePercent
//     }

//     const selfMemberPublicKey: string = this.getMemberPublicKey(shard, shard.selfPeerID)

//     if (!selfMemberPublicKey) { return }

//     let selfMemberVote: boolean = true
//     if (shard.onRemoveMemberCallback) {
//       selfMemberVote = await shard.onRemoveMemberCallback(candidateMember)
//     }

//     // Send a vote
//     this.sendTransaction(shard, {
//       [HASHGRAPH_SHARD_MESSAGE_TYPE]: HASHGRAPH_SHARD_REMOVE_MEMBER_VOTE,
//       candidateMember, votingMember: { publicKey: selfMemberPublicKey }, vote: selfMemberVote
//     })
//   }

// export {
//     functionName
// }


import { HashgraphMember } from './hashgraph-member'
import { HashgraphEvent } from './hashgraph-event'

import { addMember } from '../algorithms/hashgraph/member/add-member'
import { getMember } from '../algorithms/hashgraph/member/get-member'
import { getMemberLatestEvent } from '../algorithms/hashgraph/member/get-member-latest-event'
import { getMemberList } from '../algorithms/hashgraph/member/get-member-list'

import { addEvent } from '../algorithms/hashgraph/event/add-event'
import { findEventOrderIndex } from '../algorithms/hashgraph/event/find-event-order-index'
import { selfParentEvent } from '../algorithms/hashgraph/event/self-parent-event'
import { otherParentEvent } from '../algorithms/hashgraph/event/other-parent-event'

import { isSelfAncestorEvent } from '../algorithms/hashgraph/determine-round/is-self-ancestor-event'
import { updateChildEventAlongPathsFromSourceEvent } from '../algorithms/hashgraph/determine-round/update-child-event-along-paths-from-source-event'
import { numberOfVisitedMembersToEvent } from '../algorithms/hashgraph/determine-round/number-of-visited-members-to-event'
import { canSeeEventThroughMany } from '../algorithms/hashgraph/determine-round/can-see-event-through-many'
import { stronglySeenRoundWitnesses } from '../algorithms/hashgraph/determine-round/strongly-seen-round-witnesses'
import { determineRound } from '../algorithms/hashgraph/determine-round/determine-round'

import { highestAncestorEventFromMember } from '../algorithms/hashgraph/determine-fame/highest-ancestor-event-from-member'
import { canSeeEvent } from '../algorithms/hashgraph/determine-fame/can-see-event'
import { decideForEvent } from '../algorithms/hashgraph/determine-fame/decide-for-event'
import { determineFame } from '../algorithms/hashgraph/determine-fame/determine-fame'

import { determineOrder } from '../algorithms/hashgraph/determine-order'

import { getUnknownEventListForMember } from '../algorithms/hashgraph/gossip/get-unknown-event-list-for-member'

class Hashgraph {

    nPopulationSize: number = 0
    nPopulationSizeTotal: number = 0
    cFrequencyOfCoinRounds: number = 10
    dRoundsDelayed: number = 1

    // Member Data Table
    memberDataTable: {
      [memberUID: string]: {
        member: HashgraphMember
        orderedEventUidList: string[]
        unorderedEventUidList: string[]
      }
    } = {}

    memberDataToUidMap: {
      publicKeyMap: { [publicKey: string]: string }
      peerIdMap: { [peerId: string]: string }
    } = { publicKeyMap: {}, peerIdMap: {} }

    // Event Data Table
    eventDataTable: {
      [eventUID: string]: {
        event: HashgraphEvent,

        // Member Data
        orderIndex: number

        // Hasahgraph Data
        roundCreated: number
        roundReceived: number
        isWitness: boolean
        isFamous: boolean
        isFameDecided: boolean
        consensusTimestamp: string
      }
    } = {}

    onSelfParentOrderFoundListenerMap: { [selfParentEventUID: string]: any } = {}

    // Round Created Data
    eventRoundCreatedTable: {
      [round: number]: {
        totalMemberTable:  { [memberUID: string]: boolean }
        participantMemberTable: { [memberUID: string]: boolean }
        eventTable: { [eventUID: string ]: boolean }
        witnessEventTable: { [eventUID: string]: boolean }
        famousEventTable: { [eventUID: string]: boolean }
        notFamousEventTable: { [eventUID: string]: boolean }
      }
    } = {}

    // Round Received Data
    eventRoundReceivedTable: {
      [round: number]: {
        eventTable: {
          [eventUID: string]: {
            medianTimestamp: string
            timestampList: string[]
          }
        }
      }
    } = {}

    latestRoundCreated: number = 1
    latestRoundReceived: number = 1
    earliestRoundCreatedWithKnownConsensus: number = 0

    // Methods for Members
    addMember: typeof addMember = addMember.bind(this)
    getMember: typeof getMember = getMember.bind(this)
    getMemberLatestEvent: typeof getMemberLatestEvent = getMemberLatestEvent.bind(this)
    getMemberList: typeof getMemberList = getMemberList.bind(this)

    // Methods for Events
    addEvent: typeof addEvent = addEvent.bind(this)
    findEventOrderIndex: typeof findEventOrderIndex = findEventOrderIndex.bind(this)
    selfParentEvent: typeof selfParentEvent = selfParentEvent.bind(this)
    otherParentEvent: typeof otherParentEvent = otherParentEvent.bind(this)

    // Methods for Determine Round
    isSelfAncestorEvent: typeof isSelfAncestorEvent = isSelfAncestorEvent.bind(this)
    updateChildEventAlongPathsFromSourceEvent: typeof updateChildEventAlongPathsFromSourceEvent = updateChildEventAlongPathsFromSourceEvent.bind(this)
    numberOfVisitedMembersToEvent: typeof numberOfVisitedMembersToEvent = numberOfVisitedMembersToEvent.bind(this)
    canSeeEventThroughMany: typeof canSeeEventThroughMany = canSeeEventThroughMany.bind(this)
    stronglySeenRoundWitnesses: typeof stronglySeenRoundWitnesses = stronglySeenRoundWitnesses.bind(this)
    determineRound: typeof determineRound = determineRound.bind(this)

    // Methods for Determine Fame
    highestAncestorEventFromMember: typeof highestAncestorEventFromMember = highestAncestorEventFromMember.bind(this)
    canSeeEvent: typeof canSeeEvent = canSeeEvent.bind(this)
    decideForEvent: typeof decideForEvent = decideForEvent.bind(this)
    determineFame: typeof determineFame = determineFame.bind(this)
  
    // Methods for Determine Order
    determineOrder: typeof determineOrder = determineOrder.bind(this)

    // Methods for Gossip
    getUnknownEventListForMember: typeof getUnknownEventListForMember = getUnknownEventListForMember.bind(this)
}

interface UnknownEventTable {
  [uid: string]: {
    isAddedToList: boolean
    callbackList: Array<() => void>
  }
}

interface EventPathTable {
  [eventUID: string]: {
    hasTargetAsAncestor: boolean
    isAncestorRelationKnown: boolean
    childEventList: string[]
  }
}

interface VoteTable {
  [eventCandidateUID: string]: {
    numOfUniqueMemberVoteYes: number
    numOfUniqueMemberVoteNo: number
    numOfUniqueMemberVoters: number
    voteRecord: { [memberUID: string]: {
    hasAnEventVotedYes: boolean
    hasAnEventVotedNo: boolean
    eventTable: { [eventVoterUID: string]: { vote: boolean, hasVoted: boolean } }
    }}
    activeMemberTable: { [memberUID: string]: any }
  }
}


export {
    Hashgraph,
    UnknownEventTable,
    EventPathTable,
    VoteTable
}

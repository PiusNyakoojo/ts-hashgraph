
import { HashgraphShard } from '../../../../data-structures/hashgraph-shard'

function sendTransaction (this: HashgraphShard, transaction: any): void {
  this.transactionQueue.push(transaction)
}

export {
  sendTransaction
}






import { Hashgraph } from '../../../../data-structures/hashgraph'
import { HashgraphEvent } from '../../../../data-structures/hashgraph-event'


function highestAncestorEventFromMember (this: Hashgraph, sourceEvent: HashgraphEvent, targetEvent: HashgraphEvent): HashgraphEvent|null {

  const eventQueue: string[] = []

  const isVisitedEventTable: { [eventUID: string]: boolean } = {}
  let highestAncestorByMember: HashgraphEvent|null = null
  let highestOrderIndex: number = 0

  eventQueue.unshift(sourceEvent.selfParentHash)
  eventQueue.unshift(sourceEvent.otherParentHash)

  while (eventQueue.length > 0) {
    const hashgraphEventUID = eventQueue.pop()
    if (!hashgraphEventUID || isVisitedEventTable[hashgraphEventUID]) {
      continue
    }

    isVisitedEventTable[hashgraphEventUID] = true
    const hashgraphEvent = this.eventDataTable[hashgraphEventUID].event

    if (!hashgraphEvent) { continue }

    if (this.eventDataTable[hashgraphEvent.uid].roundCreated < this.eventDataTable[targetEvent.uid].roundCreated) {
      continue
    }

    if (hashgraphEvent.creatorUID === targetEvent.creatorUID) {
      if (highestAncestorByMember) {
        const orderIndex = this.eventDataTable[hashgraphEvent.uid].orderIndex
        if (orderIndex > highestOrderIndex) {
          highestAncestorByMember = hashgraphEvent
          highestOrderIndex = this.eventDataTable[hashgraphEvent.uid].orderIndex
        }
      } else {
        highestAncestorByMember = hashgraphEvent
        highestOrderIndex = this.eventDataTable[hashgraphEvent.uid].orderIndex
      }
    }

    if (hashgraphEvent.selfParentHash) {
      eventQueue.unshift(hashgraphEvent.selfParentHash)
    }

    if (hashgraphEvent.otherParentHash) {
      eventQueue.unshift(hashgraphEvent.otherParentHash)
    }
  }

  return highestAncestorByMember
}

export {
  highestAncestorEventFromMember
}


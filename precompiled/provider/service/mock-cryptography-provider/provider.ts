import { CryptographyProvider } from '../../service-type/cryptography'

import * as uuid from 'uuid'

class MockCryptographyProvider implements CryptographyProvider {
  hashObject(_object: any): string {
    return Math.random().toString()
  }
  verifyHash (_hash: string, _object: any): boolean {
    return true
  }
  generateKeyPair (): { privateKey: string; publicKey: string; } {
    return { privateKey: uuid.v4(), publicKey: uuid.v4() }
  }
  signMessage (_message: any, privateKey: string): any {
    return Math.random().toString() + privateKey
  }
  verifyMessage (_message: any, _signature: any, _publicKey: string): boolean {
    return true
  }
}

export {
  MockCryptographyProvider
}

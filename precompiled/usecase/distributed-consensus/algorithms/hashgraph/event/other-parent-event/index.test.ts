


import { expect } from 'chai'

import { Hashgraph } from '../../../../data-structures/hashgraph'
import { HashgraphMember } from '../../../../data-structures/hashgraph-member'

describe('Hashgraph - Other Parent Event', () => {
  it('should get other parent event', async () => {
    const hashgraph = new Hashgraph()
    const member1 = new HashgraphMember()
    const member2 = new HashgraphMember()
  
    hashgraph.addMember(member1)
    hashgraph.addMember(member2)
  
    const event1 = await member1.createEvent()
    const event2 = await member2.createEvent()
    const event3 = await member1.createEvent(null, event1.uid, event2.uid)

    await hashgraph.addEvent(event1)
    await hashgraph.addEvent(event2)
    await hashgraph.addEvent(event3)

    const otherParentEvent = hashgraph.otherParentEvent(event3)

    expect(otherParentEvent).to.deep.equal(event2)
  })
})


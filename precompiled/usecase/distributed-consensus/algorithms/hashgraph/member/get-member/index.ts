
import { Hashgraph } from '../../../../data-structures/hashgraph'
import { HashgraphMember } from '../../../../data-structures/hashgraph-member'


function getMember (this: Hashgraph, uidOrPublicKeyOrPeerId: string): HashgraphMember | null {
  const publicKeyMapUid = this.memberDataToUidMap.publicKeyMap[uidOrPublicKeyOrPeerId]
  const peerIdMapUid = this.memberDataToUidMap.peerIdMap[uidOrPublicKeyOrPeerId]

  let member: HashgraphMember | null = null

  if (this.memberDataTable[uidOrPublicKeyOrPeerId]) {
    member = this.memberDataTable[uidOrPublicKeyOrPeerId].member
  } else if (this.memberDataTable[publicKeyMapUid]) {
    member = this.memberDataTable[publicKeyMapUid].member
  } else if (this.memberDataTable[peerIdMapUid]) {
    member = this.memberDataTable[peerIdMapUid].member
  }

  return member
}

export {
    getMember
}





import { expect } from 'chai'

import { Hashgraph } from '../../../../data-structures/hashgraph'
import { HashgraphMember } from '../../../../data-structures/hashgraph-member'

describe('Hashgraph - Find Event Order Index', () => {
  it('should initialize with order index 0', async () => {
    const hashgraph = new Hashgraph()
    const member = new HashgraphMember()
    hashgraph.addMember(member)
    const event = await member.createEvent()
    await hashgraph.addEvent(event)

    expect(hashgraph.eventDataTable[event.uid].orderIndex).to.equal(0)
    expect(hashgraph.memberDataTable[member.uid].orderedEventUidList[0]).to.equal(event.uid)
  })
  it('should initialize with order index N', async () => {
    const hashgraph = new Hashgraph()
    const member1 = new HashgraphMember()
    const member2 = new HashgraphMember()
    hashgraph.addMember(member1)
    hashgraph.addMember(member2)

    let member1ExpectedEventUidList = []
    let member2ExpectedEventUidList = []

    const nNumberOfEvents = 10
    // Back and forth gossip between 2 members
    for (let i = 0; i < nNumberOfEvents; i++) {
      let member = i % 2 === 0 ? member1 : member2
      let otherMember = i % 2 === 0 ? member2 : member1

      const selfParentEvent = hashgraph.getMemberLatestEvent(member.uid)
      const otherParentEvent = hashgraph.getMemberLatestEvent(otherMember.uid)

      const selfParentHash = selfParentEvent ? selfParentEvent.uid : ''
      const otherParentHash = otherParentEvent ? otherParentEvent.uid : ''

      const event = await member.createEvent(null, selfParentHash, otherParentHash)
      await hashgraph.addEvent(event)

      if (i % 2 === 0) {
        member1ExpectedEventUidList.push(event.uid)
      } else {
        member2ExpectedEventUidList.push(event.uid)
      }
    }

    // Expect the order index to be equal to expected
    expect(hashgraph.memberDataTable[member1.uid].orderedEventUidList).to.deep.equal(member1ExpectedEventUidList)
    expect(hashgraph.memberDataTable[member2.uid].orderedEventUidList).to.deep.equal(member2ExpectedEventUidList)

    expect(hashgraph.memberDataTable[member1.uid].orderedEventUidList.length).to.equal(nNumberOfEvents / 2)
    expect(hashgraph.memberDataTable[member2.uid].orderedEventUidList.length).to.equal(nNumberOfEvents / 2)

    for (let i = 0; i < member1ExpectedEventUidList.length; i++) {
      const eventUid = member1ExpectedEventUidList[i]
      expect(hashgraph.eventDataTable[eventUid].orderIndex).to.equal(i)
    }

    for (let i = 0; i < member2ExpectedEventUidList.length; i++) {
      const eventUid = member2ExpectedEventUidList[i]
      expect(hashgraph.eventDataTable[eventUid].orderIndex).to.equal(i)
    }
  })

  it('should asynchronously initialize with order index N', async () => {
    const hashgraph = new Hashgraph()
    const member1 = new HashgraphMember()
    const member2 = new HashgraphMember()
    hashgraph.addMember(member1)
    hashgraph.addMember(member2)

    let member1ExpectedEventList = []
    let member2ExpectedEventList = []

    const nNumberOfEvents = 10
    // Back and forth gossip between 2 members
    for (let i = 0; i < nNumberOfEvents; i++) {
      let member = i % 2 === 0 ? member1 : member2
      let otherMember = i % 2 === 0 ? member2 : member1

      const selfParentEvent = hashgraph.getMemberLatestEvent(member.uid)
      const otherParentEvent = hashgraph.getMemberLatestEvent(otherMember.uid)

      const selfParentHash = selfParentEvent ? selfParentEvent.uid : ''
      const otherParentHash = otherParentEvent ? otherParentEvent.uid : ''

      const event = await member.createEvent(null, selfParentHash, otherParentHash)

      if (i % 2 === 0) {
        member1ExpectedEventList.push(event)
      } else {
        member2ExpectedEventList.push(event)
      }
    }

    // Add hashgraph event in reverse order
    for (let i = member1ExpectedEventList.length - 1; i >= 0; i--) {
      const event = member1ExpectedEventList[i]
      await hashgraph.addEvent(event)
    }

    for (let i = member2ExpectedEventList.length - 1; i >= 0; i--) {
      const event = member2ExpectedEventList[i]
      await hashgraph.addEvent(event)
    }

    const member1ExpectedEventUidList = member1ExpectedEventList.map((event) => event.uid)
    const member2ExpectedEventUidList = member2ExpectedEventList.map((event) => event.uid)

    // Expect the order index to be equal to expected
    expect(hashgraph.memberDataTable[member1.uid].orderedEventUidList).to.deep.equal(member1ExpectedEventUidList)
    expect(hashgraph.memberDataTable[member2.uid].orderedEventUidList).to.deep.equal(member2ExpectedEventUidList)

    expect(hashgraph.memberDataTable[member1.uid].orderedEventUidList.length).to.equal(nNumberOfEvents / 2)
    expect(hashgraph.memberDataTable[member2.uid].orderedEventUidList.length).to.equal(nNumberOfEvents / 2)
  })
})



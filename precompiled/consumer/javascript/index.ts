
import { HashgraphShard } from '../../usecase/distributed-consensus/data-structures/hashgraph-shard'
import { HashgraphMember } from '../../usecase/distributed-consensus/data-structures/hashgraph-member'
import { HashgraphEvent } from '../../usecase/distributed-consensus/data-structures/hashgraph-event'
import { Hashgraph } from '../../usecase/distributed-consensus/data-structures/hashgraph'

console.log('hello world')

const self: any = globalThis || {}

const LIBRARY_NAME_ID = 'TsHashgraph'

self[LIBRARY_NAME_ID] = {
    HashgraphShard,
    Hashgraph,
    HashgraphMember,
    HashgraphEvent
}



export default self[LIBRARY_NAME_ID]


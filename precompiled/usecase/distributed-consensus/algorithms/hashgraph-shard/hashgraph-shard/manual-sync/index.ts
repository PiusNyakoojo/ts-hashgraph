
import { HashgraphShard } from '../../../../data-structures/hashgraph-shard'


async function manualSync (this: HashgraphShard, targetPeerID?: string): Promise<void> {
    await this.sendGossip(shard, targetPeerID)
    await this.determineTransactionOrder(shard)
    this.manualDeleteEventHistory(shard)
  }


export {
  manualSync
}

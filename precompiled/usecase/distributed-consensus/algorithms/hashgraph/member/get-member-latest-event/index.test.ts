
import { expect } from 'chai'

import { Hashgraph } from '../../../../data-structures/hashgraph'
import { HashgraphMember } from '../../../../data-structures/hashgraph-member'

describe('Hashgraph - Get Member Latest Event', () => {
  it('should get the latest event by member', async () => {
    const hashgraph = new Hashgraph()
    const member = new HashgraphMember()
    hashgraph.addMember(member)
    const event = await member.createEvent()
    await hashgraph.addEvent(event)

    const latestEvent = hashgraph.getMemberLatestEvent(member.uid)
    expect(latestEvent).to.deep.equal(event)
  })
})



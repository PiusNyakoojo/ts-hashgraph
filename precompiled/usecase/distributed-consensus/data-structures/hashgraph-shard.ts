
// import { HashgraphMember } from './hashgraph-member'
import { Hashgraph } from './hashgraph'

import { addEventListener } from '../algorithms/hashgraph-shard/hashgraph-shard/add-event-listener'
import { addEventListenerForSendGossip } from '../algorithms/hashgraph-shard/gossip/add-event-listener-for-send-gossip'
import { addEventListenerForConsensusState } from '../algorithms/hashgraph-shard/virtual-voting/add-event-listener-for-consensus-state'
import { addEventListenerForTransactionOrder } from '../algorithms/hashgraph-shard/virtual-voting/add-event-listener-for-transaction-order'

import { sendTransaction } from '../algorithms/hashgraph-shard/hashgraph-shard/send-transaction'
import { receiveGossip } from '../algorithms/hashgraph-shard/gossip/receive-gossip'

import { addMember } from '../algorithms/hashgraph-shard/member/add-member'
import { getMember } from '../algorithms/hashgraph-shard/member/get-member'


class HashgraphShard {
    hashgraph: Hashgraph = new Hashgraph()

    selfPeerID: string = ''

    // addMemberCandidateTable: { [candidateUID: string]: {
    //     candidate: HashgraphMember, roundNominated: number, voteTable: { [voterUID: string]: boolean }, minimumVotePercent: number
    // } } = {}
    // removeMemberCandidateTable: { [candidateUID: string]: {
    //     candidate: HashgraphMember, roundNominated: number, voteTable: { [voterUID: string]: boolean }, minimumVotePercent: number
    // } } = {}

    // defaultMinimumVotePercent: number = 66 // default to at least 2/3 of current population to add or remove members

    transactionQueue: any[] = []

    // latestRoundReceived: number = -1
    // stateTable: { [roundReceived: number]: any } = {}

    // syncInterval?: any|number
    // syncTickRate: number = 1000 // default to 1000 milliseconds per syncs
    // isStartedAutoSync: boolean = false

    // deleteEventHistoryInterval?: any|number
    // elapsedTimeToDeleteEventHistory: number = 20 // default to 20 seconds to delete history
    // isStartedAutoDelete: boolean = false

    eventListenerTable: {
      [eventId: string]: {
        [eventListenerId: string]: any
      }
    } = {}

    public addEventListener: typeof addEventListener = addEventListener.bind(this)
    protected addEventListenerForSendGossip: typeof addEventListenerForSendGossip = addEventListenerForSendGossip.bind(this)
    protected addEventListenerForTransactionOrder: typeof addEventListenerForTransactionOrder = addEventListenerForTransactionOrder.bind(this)
    protected addEventListenerForConsensusState: typeof addEventListenerForConsensusState = addEventListenerForConsensusState.bind(this)

    sendTransaction: typeof sendTransaction = sendTransaction.bind(this)
    receiveGossip: typeof receiveGossip = receiveGossip.bind(this)

    addMember: typeof addMember = addMember.bind(this)
    getMember: typeof getMember = getMember.bind(this)

}

export {
    HashgraphShard
}


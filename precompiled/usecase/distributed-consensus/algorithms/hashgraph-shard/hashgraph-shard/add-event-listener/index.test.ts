
import { expect } from 'chai'

import { HashgraphShard } from '../../../../data-structures/hashgraph-shard'

import { sendGossipEventId } from '../../gossip/add-event-listener-for-send-gossip'

describe('Hashgraph Shard - Add Event Listener', () => {
  it('should add event listener for send_gossip event', async () => {
    const shard = new HashgraphShard()

    expect(Object.keys(shard.eventListenerTable[sendGossipEventId] || {}).length).to.equal(0)

    const eventListenerId = shard.addEventListener(sendGossipEventId, () => {
      //
    })

    expect(eventListenerId).to.not.be.undefined
    expect(Object.keys(shard.eventListenerTable[sendGossipEventId]).length).to.equal(1)
  })
})

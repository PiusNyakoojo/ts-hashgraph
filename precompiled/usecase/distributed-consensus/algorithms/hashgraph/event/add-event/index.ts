
import { Hashgraph } from '../../../../data-structures/hashgraph'
import { HashgraphEvent } from '../../../../data-structures/hashgraph-event'

async function addEvent (this: Hashgraph, event: HashgraphEvent): Promise<boolean> {

    if (!event) {
      return false
    }

    // Ensure event isn't already added.
    if (this.eventDataTable[event.uid]) {
      return false
    }

    // Get event creator member
    const eventCreatorMember = this.memberDataTable[event.creatorUID].member

    if (!eventCreatorMember) {
      return false
    }

    // Ensure event is valid
    const isValidEvent = await eventCreatorMember.verifyEvent(event)
    if (!isValidEvent) {
      return false
    }

    // Initialize Event Data
    this.eventDataTable[event.uid] = {
      event,
      orderIndex: -1,
      roundCreated: -1,
      roundReceived: -1,
      isWitness: false,
      isFamous: false,
      isFameDecided: false,
      consensusTimestamp: ''
    }

    this.findEventOrderIndex(event)
    this.determineRound(event)

    return true
}



export {
  addEvent
}





import { expect } from 'chai'

import { createHashgraphExample } from '../../hashgraph/create-hashgraph/create-hashgraph-example'
import { exampleHashgraph2 } from '../../hashgraph/create-hashgraph/create-hashgraph-example.test'


describe('Hashgraph - Strongly Seen Round Witnesses', () => {
  it('should see through many for N events', async () => {
    const hashgraph = await createHashgraphExample(exampleHashgraph2)

    const memberUidList = hashgraph.getMemberList().map((member) =>  member.uid)

    for (let i = 0; i < memberUidList.length; i++) {
      const memberUid = memberUidList[i]
      const sourceEvent  = hashgraph.getMemberLatestEvent(memberUid)!

      const stronglySeenRoundWitnesses = hashgraph.stronglySeenRoundWitnesses(sourceEvent, 1)
      expect(stronglySeenRoundWitnesses.length).to.equal(4)
    }
  })
})



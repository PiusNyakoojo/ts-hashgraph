

import { Hashgraph } from '../../../../data-structures/hashgraph'
import { HashgraphMember } from '../../../../data-structures/hashgraph-member'

async function createHashgraphExample (hashgraphRepresentation: Array<string[]>): Promise<Hashgraph> {
  
  const hashgraph = new Hashgraph()
  const memberList = []
  let numberOfEvents = 0
  let mostNumberOfEventsByMember = 0

  // Create member list
  for (let i = 0; i < hashgraphRepresentation.length; i++) {
    const member = new HashgraphMember({ publicKey: String.fromCharCode(97 + i) })
    memberList.push(member)
    hashgraph.addMember(member)
    numberOfEvents += hashgraphRepresentation[i].length
    if (mostNumberOfEventsByMember < hashgraphRepresentation[i].length) {
      mostNumberOfEventsByMember = hashgraphRepresentation[i].length
    }
  }

  let numberOfEventsAdded = 0
  const eventTable: { [eventRepresentationId: string]: string } = {}

  loop1:
  while (numberOfEventsAdded < numberOfEvents) {
    loop2:
    for (let i = 0; i < mostNumberOfEventsByMember; i++) {
      loop3:
      for (let j = 0; j < memberList.length; j++) {
        let selfMemberId = String.fromCharCode(97 + j)
        const member = memberList[j]

        const eventRepresentationId = `${selfMemberId}-${i}`
        const selfParentEventRepresentationId = `${selfMemberId}-${i - 1}`
        const otherParentEventRepresentationId = hashgraphRepresentation[j][i]

        if (eventTable[eventRepresentationId]) {
          continue loop3
        }

        if ((!eventTable[selfParentEventRepresentationId] || !eventTable[otherParentEventRepresentationId]) && otherParentEventRepresentationId != '') {
          continue loop3
        }

        const event = await member.createEvent(null, eventTable[selfParentEventRepresentationId] , eventTable[otherParentEventRepresentationId])
        await hashgraph.addEvent(event)

        eventTable[eventRepresentationId] = event.uid

        numberOfEventsAdded = Object.keys(hashgraph.eventDataTable).length
      }
    }
  }

  return hashgraph
}


export {
  createHashgraphExample
}

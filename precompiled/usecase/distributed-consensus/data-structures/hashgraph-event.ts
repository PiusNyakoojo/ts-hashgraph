
// import { verifyEvent } from '../algorithms/hashgraph-event/1-verify-event'

class HashgraphEvent {
    uid: string = ''
    payload: any = ''
    selfParentHash: string = ''
    otherParentHash: string = ''
    timestamp: string = ''
    creatorUID: string = ''
    signature: string = ''
  
    constructor (payload: any, creatorUID: string, selfParentHash?: string, otherParentHash?: string) {
        this.timestamp = Date.now().toString()

        this.payload = payload
        this.creatorUID = creatorUID
        this.selfParentHash = selfParentHash || ''
        this.otherParentHash = otherParentHash || ''
    }

   // verifyEvent = verifyEvent.bind(this)
}

export {
    HashgraphEvent
}


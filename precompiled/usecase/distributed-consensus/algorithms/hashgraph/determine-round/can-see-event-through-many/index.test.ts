
import { expect } from 'chai'

import { createHashgraphExample } from '../../hashgraph/create-hashgraph/create-hashgraph-example'
import { exampleHashgraph2 } from '../../hashgraph/create-hashgraph/create-hashgraph-example.test'


describe('Hashgraph - Can See Event Through Many', () => {
  it('should see through many for N events', async () => {
    const hashgraph = await createHashgraphExample(exampleHashgraph2)

    const memberUidList = hashgraph.getMemberList().map((member) =>  member.uid)

    for (let i = 0; i < memberUidList.length; i++) {
      const memberUid = memberUidList[i]
      const sourceEvent  = hashgraph.getMemberLatestEvent(memberUid)!

      for (let j = 0; j < memberUidList.length; j++) {
        const targetMemberUid = memberUidList[j]
        const targetEventUid = hashgraph.memberDataTable[targetMemberUid].orderedEventUidList[0]
        const targetEvent = hashgraph.eventDataTable[targetEventUid].event
        const canSeeEventThroughMany = hashgraph.canSeeEventThroughMany(sourceEvent, targetEvent, memberUidList)

        expect(canSeeEventThroughMany).to.be.equal(true)
      }
    }
  })
  it('should not see through many for N events', async () => {
    const hashgraph = await createHashgraphExample(exampleHashgraph2)

    const memberUidList = hashgraph.getMemberList().map((member) =>  member.uid)

    for (let i = 0; i < memberUidList.length; i++) {
      const memberUid = memberUidList[i]
      const sourceEventUid = hashgraph.memberDataTable[memberUid].orderedEventUidList[0]
      const sourceEvent  = hashgraph.eventDataTable[sourceEventUid].event

      for (let j = 0; j < memberUidList.length; j++) {
        const targetMemberUid = memberUidList[j]
        const targetEventUid = hashgraph.memberDataTable[targetMemberUid].orderedEventUidList[0]
        const targetEvent = hashgraph.eventDataTable[targetEventUid].event
        const canSeeEventThroughMany = hashgraph.canSeeEventThroughMany(sourceEvent, targetEvent, memberUidList)

        expect(canSeeEventThroughMany).to.be.equal(false)
      }
    }
  })
})


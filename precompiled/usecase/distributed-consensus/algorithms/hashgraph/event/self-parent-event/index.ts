


import { Hashgraph } from '../../../../data-structures/hashgraph'
import { HashgraphEvent } from '../../../../data-structures/hashgraph-event'


function selfParentEvent (this: Hashgraph, event: HashgraphEvent): HashgraphEvent|null {

    if (!this.memberDataTable[event.creatorUID]) {
      return null
    }

    const selfParentEventUID = event.selfParentHash

    if (!selfParentEventUID) {
      return null
    }

    return this.eventDataTable[selfParentEventUID].event
  }

export {
  selfParentEvent
}


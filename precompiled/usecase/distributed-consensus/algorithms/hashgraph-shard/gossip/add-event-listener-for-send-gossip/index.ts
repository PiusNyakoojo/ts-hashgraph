

import { HashgraphShard } from '../../../../data-structures/hashgraph-shard'
import { HashgraphEvent } from '../../../../data-structures/hashgraph-event'

import * as uuid from 'uuid'

const sendGossipEventId = 'send_gossip'
declare type sendGossipEventListenerFunctionType = (event: { targetPeerId: string, eventList: HashgraphEvent[] }) => Promise<void> | void



function addEventListenerForSendGossip (this: HashgraphShard, eventListenerFunction: sendGossipEventListenerFunctionType): string {
  const eventListenerId = uuid.v4()

  if (!this.eventListenerTable[sendGossipEventId]) {
    this.eventListenerTable[sendGossipEventId] = {}
  }

  this.eventListenerTable[sendGossipEventId][eventListenerId] = eventListenerFunction

  return eventListenerId
}

export {
  addEventListenerForSendGossip,
  sendGossipEventListenerFunctionType,
  sendGossipEventId
}


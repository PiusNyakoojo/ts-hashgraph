

import { HashgraphMember } from '../../../data-structures/hashgraph-member'
import { HashgraphEvent } from '../../../data-structures/hashgraph-event'


async function verifyEvent (this: HashgraphMember, event: HashgraphEvent): Promise<boolean> {

  const eventMessage = {
    payload: event.payload,
    creatorUID: event.creatorUID,
    selfParentHash: event.selfParentHash,
    otherParentHash: event.otherParentHash,
    timestamp: event.timestamp
  }

  // Validate the signature
  const isValidSignature = await this.cryptographyProvider.verifyMessage(eventMessage, event.signature, this.publicKey!)

  // Validate the cryptographic hash
  const isValidHash = await this.cryptographyProvider.verifyHash(event.uid, {
    ...eventMessage, signature: event.signature
  })

  return isValidSignature && isValidHash
}

export {
  verifyEvent
}





import { Hashgraph, EventPathTable } from '../../../../data-structures/hashgraph'
// import { HashgraphMember } from '../../../data-structures/hashgraph-member'
// import { HashgraphEvent } from '../../../data-structures/hashgraph-event'


function updateChildEventAlongPathsFromSourceEvent (this: Hashgraph, eventPathTable: EventPathTable, sourceEventUID: string, isVisitedMemberTable: { [memberUID: string]: boolean }) {
    const childEventList = eventPathTable[sourceEventUID].childEventList

    const childEventQueue: string[] = []
    const isVisitedChildEventTable: { [eventUID: string]: boolean } = {}
    for (const childEventUID of childEventList) {
      let eventUID: string = childEventUID

      childEventQueue.unshift(eventUID)

      while (childEventQueue.length > 0) {
        eventUID = childEventQueue.pop() || ''
        if (!eventUID || isVisitedChildEventTable[eventUID]) {
          continue
        }

        isVisitedChildEventTable[eventUID] = true

        if (!eventPathTable[eventUID]) {
          eventPathTable[eventUID] = { hasTargetAsAncestor: false, isAncestorRelationKnown: false, childEventList: [] }
        }

        if (eventPathTable[eventUID].isAncestorRelationKnown) {
          continue
        }

        eventPathTable[eventUID].hasTargetAsAncestor = true
        eventPathTable[eventUID].isAncestorRelationKnown = true

        const hashgraphEvent = this.eventDataTable[eventUID].event

        if (!hashgraphEvent) { continue }

        isVisitedMemberTable[hashgraphEvent.creatorUID] = true

        const nextChildEventList = eventPathTable[eventUID].childEventList

        for (const nextChildEventUID of nextChildEventList) {
          childEventQueue.unshift(nextChildEventUID)
        }
      }
    }
  }

export {
  updateChildEventAlongPathsFromSourceEvent
}



// import { HashgraphShard } from '../../../data-structures/hashgraph-shard'

// function functionName (this: HashgraphShard) {
//     //
// }

// removeMember (shard: HashgraphShard, candidateMember: HashgraphShardMember, skipVoting?: boolean, round?: number): void {
//     const publicKey = candidateMember.publicKey
//     const hashgraphMember = this.hashgraphUsecase.getMember(shard.hashgraph, publicKey)

//     // Ensure candidate is already a member.
//     if (!hashgraphMember) { return }

//     if (shard.hashgraph.latestRoundCreated < 2 ||  skipVoting) {
//       this.hashgraphUsecase.removeMember(shard.hashgraph, hashgraphMember.uid, round)

//       const peerID = shard.publicKeyPeerIDMap[publicKey]
//       delete shard.connectedPeerMap[peerID]

//       if (shard.onMemberUpdateCallback) {
//         shard.onMemberUpdateCallback(candidateMember, MEMBER_UPDATE_TYPE_REMOVED)
//       }
//       delete shard.removeMemberCandidateTable[publicKey]
//     } else {
//       this.sendTransaction(shard, {
//         [HASHGRAPH_SHARD_MESSAGE_TYPE]: HASHGRAPH_SHARD_REMOVE_MEMBER_NOMINATION,
//         candidateMember
//       })
//     }
//   }

// export {
//     functionName
// }

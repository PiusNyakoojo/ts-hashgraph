

import { Hashgraph } from '../../../../data-structures/hashgraph'
import { HashgraphEvent } from '../../../../data-structures/hashgraph-event'

function findEventOrderIndex (this: Hashgraph, event: HashgraphEvent) {
  let orderIndex: number = -1
  let selfParentOrderIndex: number = -1

  if (!event.selfParentHash) {
    orderIndex = 0
  } else if (this.eventDataTable[event.selfParentHash]) {
    selfParentOrderIndex = this.eventDataTable[event.selfParentHash].orderIndex
  }

  if (selfParentOrderIndex >= 0) {
    orderIndex = selfParentOrderIndex + 1
  }

  const eventCreatorMember = this.memberDataTable[event.creatorUID].member
  if (!eventCreatorMember) {
    return false
  }

  // Ensure event is added after its self parent
  if (orderIndex < 0) {
    const unorderedEventIndex = this.memberDataTable[eventCreatorMember.uid].unorderedEventUidList.length
    this.memberDataTable[eventCreatorMember.uid].unorderedEventUidList.push(event.uid)

    this.onSelfParentOrderFoundListenerMap[event.selfParentHash] = (selfParentOrderIndex: number) => {
      if (this.eventDataTable[event.uid].orderIndex >= 0) { return }

      this.memberDataTable[eventCreatorMember.uid].unorderedEventUidList.splice(unorderedEventIndex, 1)
      this.eventDataTable[event.uid].orderIndex = selfParentOrderIndex + 1
      this.memberDataTable[eventCreatorMember.uid].orderedEventUidList.splice(selfParentOrderIndex + 1, 0, event.uid)

      if (this.onSelfParentOrderFoundListenerMap[event.uid]) {
        this.onSelfParentOrderFoundListenerMap[event.uid](selfParentOrderIndex + 1)
      }
    }
  // Set the order index of the event
  } else if (orderIndex >= 0) {
    if (this.eventDataTable[event.uid].orderIndex >= 0) { return false }

    this.eventDataTable[event.uid].orderIndex = orderIndex
    this.memberDataTable[eventCreatorMember.uid].orderedEventUidList.splice(orderIndex, 0, event.uid)

    if (this.onSelfParentOrderFoundListenerMap[event.uid]) {
      this.onSelfParentOrderFoundListenerMap[event.uid](orderIndex)
    }
  }

  return orderIndex
}

export {
  findEventOrderIndex
}








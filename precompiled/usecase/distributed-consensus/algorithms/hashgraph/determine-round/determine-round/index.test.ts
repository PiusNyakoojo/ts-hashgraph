
import { expect } from 'chai'

import { createHashgraphExample } from '../../hashgraph/create-hashgraph/create-hashgraph-example'
import { exampleHashgraph2 } from '../../hashgraph/create-hashgraph/create-hashgraph-example.test'


describe('Hashgraph - Determine Round', () => {
  it('should determine round for N events', async () => {
    const hashgraph = await createHashgraphExample(exampleHashgraph2)

    const memberUidList = hashgraph.getMemberList().map((member) =>  member.uid)

    for (let i = 0; i < memberUidList.length; i++) {
      const memberUid = memberUidList[i]
      const sourceEvent  = hashgraph.getMemberLatestEvent(memberUid)!
      expect(hashgraph.eventDataTable[sourceEvent.uid].roundCreated).to.be.greaterThanOrEqual(3)
    }
  })
})



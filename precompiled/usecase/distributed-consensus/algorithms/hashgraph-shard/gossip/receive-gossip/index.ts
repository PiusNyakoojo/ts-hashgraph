
import { HashgraphShard } from '../../../../data-structures/hashgraph-shard'
import { HashgraphEvent } from '../../../../data-structures/hashgraph-event'

async function receiveGossip (this: HashgraphShard, eventList: HashgraphEvent[]): Promise<void> {
  const otherParentEvent = eventList[eventList.length - 1]

  // Add sync events and other parent event to the hashgraph.
  for (const event of eventList) {
    await this.hashgraph.addEvent(event)
  }

  // Create new event to commemorate the sync.
  const selfMember = this.hashgraph.getMember(this.selfPeerID)
  if (!selfMember) { return }

  const latestSelfEvent = this.hashgraph.getMemberLatestEvent(selfMember.uid)
  const latestSelfEventHash = latestSelfEvent ? latestSelfEvent.uid : ''
  const otherParentHash = otherParentEvent ? otherParentEvent.uid : ''

  const newEvent = await selfMember.createEvent(this.transactionQueue.slice(), latestSelfEventHash, otherParentHash)
  this.transactionQueue = []

  // Add the new event to the hashgraph.
  await this.hashgraph.addEvent(newEvent)
}

export {
  receiveGossip
}



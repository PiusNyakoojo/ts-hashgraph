


import { Hashgraph, EventPathTable } from '../../../../data-structures/hashgraph'
import { HashgraphEvent } from '../../../../data-structures/hashgraph-event'


function numberOfVisitedMembersToEvent (this: Hashgraph, sourceEvent: HashgraphEvent, targetEvent: HashgraphEvent, memberUIDList: string[]): number {
  const eventQueue: string[] = []
  const eventPathTable: EventPathTable = {}

  const isVisitedEventTable: { [eventUID: string]: boolean } = {}
  const isVisitedMemberTable: { [memberUID: string]: boolean } = {}

  let nearestAncestorByMember: HashgraphEvent|null = null
  const targetEventData = this.eventDataTable[targetEvent.uid]

  isVisitedMemberTable[sourceEvent.creatorUID] = true

  eventPathTable[sourceEvent.selfParentHash] = {
    hasTargetAsAncestor: false, isAncestorRelationKnown: false, childEventList: [sourceEvent.uid]
  }

  eventPathTable[sourceEvent.otherParentHash] = {
    hasTargetAsAncestor: false, isAncestorRelationKnown: false, childEventList: [sourceEvent.uid]
  }

  eventQueue.unshift(sourceEvent.selfParentHash)
  eventQueue.unshift(sourceEvent.otherParentHash)


  while (eventQueue.length > 0) {
    const hashgraphEventUID = eventQueue.pop()
    if (!hashgraphEventUID || isVisitedEventTable[hashgraphEventUID]) {
      continue
    }

    isVisitedEventTable[hashgraphEventUID] = true
    const hashgraphEvent = this.eventDataTable[hashgraphEventUID].event
    const hashgraphEventData = this.eventDataTable[hashgraphEventUID]

    if (!hashgraphEvent) { continue }

    if (hashgraphEventData.roundCreated < targetEventData.roundCreated) {
      continue
    }

    // Record the events that have been visited by the current event if the target event
    // is a self ancestor. Break from the loop if the target event is not a self ancestor
    // of the current event by the same member.
    if (hashgraphEvent.creatorUID === targetEvent.creatorUID) {
      if (!nearestAncestorByMember) {
        nearestAncestorByMember = hashgraphEvent

        const isTargetSelfAncestorOfNearest = this.isSelfAncestorEvent(nearestAncestorByMember, targetEvent)
        if (!isTargetSelfAncestorOfNearest) {
          break
        }
      }

      const isTargetSelfAncestor = this.isSelfAncestorEvent(hashgraphEvent, targetEvent)
      eventPathTable[hashgraphEvent.uid].hasTargetAsAncestor = isTargetSelfAncestor
      eventPathTable[hashgraphEvent.uid].isAncestorRelationKnown = true

      if (isTargetSelfAncestor) {
        isVisitedMemberTable[hashgraphEvent.creatorUID] = true
        this.updateChildEventAlongPathsFromSourceEvent(eventPathTable, hashgraphEvent.uid, isVisitedMemberTable)
      } else {
        continue
      }
    }

    // Set the self parent event to have the same member list as the current event. Also check if the self
    // parent event is known to have the target as an ancestor to ensure the current event's member list is
    // recorded/visited.
    if (hashgraphEvent.selfParentHash) {
      if (!eventPathTable[hashgraphEvent.selfParentHash]) {
        eventPathTable[hashgraphEvent.selfParentHash] = { hasTargetAsAncestor: false, isAncestorRelationKnown: false, childEventList: [] }
      }
      eventPathTable[hashgraphEvent.selfParentHash].childEventList.push(hashgraphEvent.uid)

      // If the self parent event has already been visited, and the ancestor relation is known to be true, then
      // update the child events of the current event to also see/know about the target ancestor event.
      //
      // If the self parent event does not yet know if the target event is an ancestor, because we have
      // added this event as a child, if the target event turns out to be an ancestor, then it will traverse
      // through its child events and know about the child events of this current event as well.
      if (isVisitedEventTable[hashgraphEvent.selfParentHash]) {
        if (eventPathTable[hashgraphEvent.selfParentHash].isAncestorRelationKnown) {
          if (eventPathTable[hashgraphEvent.selfParentHash].hasTargetAsAncestor) {
            // console.log('Self parent member already visited. Now updating.')
            isVisitedMemberTable[hashgraphEvent.creatorUID] = true
            this.updateChildEventAlongPathsFromSourceEvent(eventPathTable, hashgraphEvent.uid, isVisitedMemberTable)
          }
        }
      }

      eventQueue.unshift(hashgraphEvent.selfParentHash)
    }

    // Set the other parent event to have the same member list as the current event. Also check if the other
    // parent event is known to have the target as an ancestor to ensure the current event's member list is
    // recorded/visited.
    if (hashgraphEvent.otherParentHash) {
      if (!eventPathTable[hashgraphEvent.otherParentHash]) {
        eventPathTable[hashgraphEvent.otherParentHash] = { hasTargetAsAncestor: false, isAncestorRelationKnown: false, childEventList: [] }
      }
      eventPathTable[hashgraphEvent.otherParentHash].childEventList.push(hashgraphEvent.uid)

      // Similar reasoning is applied here as to the self parent condition.
      if (isVisitedEventTable[hashgraphEvent.otherParentHash]) {
        if (eventPathTable[hashgraphEvent.otherParentHash].isAncestorRelationKnown) {
          if (eventPathTable[hashgraphEvent.otherParentHash].hasTargetAsAncestor) {
            isVisitedMemberTable[hashgraphEvent.creatorUID] = true
            this.updateChildEventAlongPathsFromSourceEvent(eventPathTable, hashgraphEvent.uid, isVisitedMemberTable)
          }
        }
      }

      eventQueue.unshift(hashgraphEvent.otherParentHash)
    }
  }

  let numVisitedMembers = 0

  for (const memberUID of memberUIDList) {
    numVisitedMembers += isVisitedMemberTable[memberUID] ? 1 : 0
  }

  return numVisitedMembers
}

export {
  numberOfVisitedMembersToEvent
}




interface CryptographyProvider {
    generateKeyPair (): { privateKey: string, publicKey: string }
    signMessage (message: any, privateKey: string): any
    verifyMessage (message: any, signature: any, publicKey: string): boolean

    hashObject (object: any): string
    verifyHash (hash: string, object: any): boolean
}

export {
    CryptographyProvider
}



import { Hashgraph } from '../../../../data-structures/hashgraph'
import { HashgraphEvent } from '../../../../data-structures/hashgraph-event'

function canSeeEvent (this: Hashgraph, sourceEvent: HashgraphEvent, targetEvent: HashgraphEvent): boolean {
  const isSelfAncestor = this.isSelfAncestorEvent(sourceEvent, targetEvent)

  if (isSelfAncestor) {
    return true
  }

  const ancestorEvent = this.highestAncestorEventFromMember(sourceEvent, targetEvent)
  let isOtherAncestor = false
  if (ancestorEvent) {
    isOtherAncestor = this.isSelfAncestorEvent(ancestorEvent, targetEvent)
  }

  return isOtherAncestor
}

export {
  canSeeEvent
}


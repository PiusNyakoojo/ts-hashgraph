


import { VoteTable } from '../../../../data-structures/hashgraph'
import { HashgraphEvent } from '../../../../data-structures/hashgraph-event'

function voteForEvent (voteTable: VoteTable, candidateEvent: HashgraphEvent, voterEvent: HashgraphEvent, vote: boolean): void {

  if (!voteTable[candidateEvent.uid].voteRecord[voterEvent.creatorUID].eventTable[voterEvent.uid].hasVoted) {
    const isFromActiveMember = voteTable[candidateEvent.uid].activeMemberTable[voterEvent.creatorUID] ? true : false
    const hasMemberNotVotedYes = !voteTable[candidateEvent.uid].voteRecord[voterEvent.creatorUID].hasAnEventVotedYes
    const hasMemberNotVotedNo = !voteTable[candidateEvent.uid].voteRecord[voterEvent.creatorUID].hasAnEventVotedNo

    if (vote === true && hasMemberNotVotedYes && isFromActiveMember) {
      voteTable[candidateEvent.uid].numOfUniqueMemberVoteYes++
      voteTable[candidateEvent.uid].voteRecord[voterEvent.creatorUID].hasAnEventVotedYes = true
    } else if (vote === false && hasMemberNotVotedNo && isFromActiveMember) {
      voteTable[candidateEvent.uid].numOfUniqueMemberVoteNo++
      voteTable[candidateEvent.uid].voteRecord[voterEvent.creatorUID].hasAnEventVotedNo = true
    }

    const hasNotVoted = hasMemberNotVotedYes && hasMemberNotVotedNo && isFromActiveMember
    if (hasNotVoted) {
      voteTable[candidateEvent.uid].numOfUniqueMemberVoters++
    }

    voteTable[candidateEvent.uid].voteRecord[voterEvent.creatorUID].eventTable[voterEvent.uid].vote = vote
    voteTable[candidateEvent.uid].voteRecord[voterEvent.creatorUID].eventTable[voterEvent.uid].hasVoted = true
  }
}


export {
  voteForEvent
}


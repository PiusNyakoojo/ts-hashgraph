
import { expect } from 'chai'

import { Hashgraph } from '../../../../data-structures/hashgraph'
import { HashgraphMember } from '../../../../data-structures/hashgraph-member'

describe('Hashgraph - Add Member', () => {
  it('should have 0 members to start', () => {
    const hashgraph = new Hashgraph()
    expect(hashgraph.nPopulationSize).to.equal(0)
  })
  it('should add 1 member', () => {
    const hashgraph = new Hashgraph()
    const member = new HashgraphMember()
    hashgraph.addMember(member)
    expect(hashgraph.nPopulationSize).to.equal(1)
  })
  it('should add N members', () => {
    const hashgraph = new Hashgraph()
    const nNumberOfMembers = 10
    for (let i = 0; i < nNumberOfMembers; i++) {
      const member = new HashgraphMember()
      hashgraph.addMember(member)
    }
    expect(hashgraph.nPopulationSize).to.equal(nNumberOfMembers)
  })
  it('should add N members in round 1', () => {
    const hashgraph = new Hashgraph()
    const nNumberOfMembers = 10
    const rRoundNumber = 1
    for (let i = 0; i < nNumberOfMembers; i++) {
      const member = new HashgraphMember()
      hashgraph.addMember(member)
    }
    const numberOfRoundCreatedMembers = Object.keys(hashgraph.eventRoundCreatedTable[rRoundNumber].totalMemberTable).length
    expect(numberOfRoundCreatedMembers).to.deep.equal(nNumberOfMembers)
  })
  it('should add N members in round 2', () => {
    const hashgraph = new Hashgraph()
    const nNumberOfMembers = 10
    const rRoundNumber = 2
    for (let i = 0; i < nNumberOfMembers; i++) {
      const member = new HashgraphMember()
      hashgraph.addMember(member, rRoundNumber)
    }
    const numberOfRoundCreatedMembers = Object.keys(hashgraph.eventRoundCreatedTable[rRoundNumber].totalMemberTable).length
    expect(numberOfRoundCreatedMembers).to.deep.equal(nNumberOfMembers)
  })
  it('should add N members in round R', () => {
    const hashgraph = new Hashgraph()
    const nNumberOfMembers = 10
    const rRoundNumber = 10
    for (let i = 0; i < nNumberOfMembers; i++) {
      const member = new HashgraphMember()
      hashgraph.addMember(member, rRoundNumber)
    }
    const numberOfRoundCreatedMembers = Object.keys(hashgraph.eventRoundCreatedTable[rRoundNumber].totalMemberTable).length
    expect(numberOfRoundCreatedMembers).to.deep.equal(nNumberOfMembers)
  })
})

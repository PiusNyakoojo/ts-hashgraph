


import { Hashgraph } from '../../../../data-structures/hashgraph'
import { HashgraphEvent } from '../../../../data-structures/hashgraph-event'

function otherParentEvent (this: Hashgraph, event: HashgraphEvent): HashgraphEvent | null {
  const otherParentEventUID = event.otherParentHash
  if (!otherParentEventUID || !this.eventDataTable[otherParentEventUID]) {
    return null
  }

  return this.eventDataTable[otherParentEventUID].event
}

export {
  otherParentEvent
}




import { expect } from 'chai'

import { createHashgraphExample } from '../../hashgraph/create-hashgraph/create-hashgraph-example'
import { exampleHashgraph2 } from '../../hashgraph/create-hashgraph/create-hashgraph-example.test'

describe('Hashgraph - Is Self Ancestor Event', () => {
  it('should determine self ancestor for event N', async () => {
    const hashgraph = await createHashgraphExample(exampleHashgraph2)

    const memberA = hashgraph.getMember('a')!
    const memberB = hashgraph.getMember('b')!
    const memberAOrderedEventUidList = hashgraph.memberDataTable[memberA.uid].orderedEventUidList
    const memberBOrderedEventUidList = hashgraph.memberDataTable[memberB.uid].orderedEventUidList
    const memberLatestEvent = hashgraph.getMemberLatestEvent(memberA.uid)!

    for (let i = 0; i < memberAOrderedEventUidList.length; i++) {
      const targetEventUid = memberAOrderedEventUidList[i]
      const targetEvent = hashgraph.eventDataTable[targetEventUid].event
      const isSelfAncestor = hashgraph.isSelfAncestorEvent(memberLatestEvent, targetEvent)
      expect(isSelfAncestor).to.be.equal(true)
    }

    for (let i = 0; i < memberBOrderedEventUidList.length; i++) {
      const targetEventUid = memberBOrderedEventUidList[i]
      const targetEvent = hashgraph.eventDataTable[targetEventUid].event
      const isSelfAncestor = hashgraph.isSelfAncestorEvent(memberLatestEvent, targetEvent)
      expect(isSelfAncestor).to.be.equal(false)
    }
  })
})

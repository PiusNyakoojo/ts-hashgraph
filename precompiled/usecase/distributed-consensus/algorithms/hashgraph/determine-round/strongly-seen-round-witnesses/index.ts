


import { Hashgraph } from '../../../../data-structures/hashgraph'
import { HashgraphEvent } from '../../../../data-structures/hashgraph-event'


function stronglySeenRoundWitnesses (this: Hashgraph, event: HashgraphEvent, round: number): HashgraphEvent[] {
  if (!this.eventRoundCreatedTable[round]) {
    return []
  }

  const witnessEventList = Object.keys(this.eventRoundCreatedTable[round].witnessEventTable).map((eventUID) => this.eventDataTable[eventUID].event)
  // const memberUIDList = Object.keys(hashgraph.eventRoundCreatedTable[round].memberTable)
  const memberUIDList = this.getMemberList(round).map((member) => member.uid)
  // const memberUIDList = Object.keys(hashgraph.memberTable)

  const stronglySeenWitnessList: HashgraphEvent[] = []

  for (let i = 0; i < witnessEventList.length; i++) {
    const witnessEvent = witnessEventList[i]

    const isStronglySeenWitness = this.canSeeEventThroughMany(event, witnessEvent, memberUIDList)

    if (isStronglySeenWitness) {
      stronglySeenWitnessList.push(witnessEvent)
    }
  }

  return stronglySeenWitnessList
}

export {
  stronglySeenRoundWitnesses
}


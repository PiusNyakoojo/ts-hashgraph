


import { expect } from 'chai'

import { createHashgraphExample } from './create-hashgraph-example'


// Example from https://www.youtube.com/watch?v=wgwYU1Zr9Tg&list=PLAKqWAegrR6MHQupsPvJ0EDfuC2ViDSxX&ab_channel=Hedera
const exampleHashgraph2 = [
  ['', 'b-1', 'd-4', 'c-1', 'b-4', 'b-6', 'b-8', 'b-9'], // 8 events
  ['', 'd-1', 'c-0', 'd-3', 'd-4', 'a-4', 'd-6', 'a-5', 'a-5', 'c-3', 'a-7', 'd-10'], // 12 events
  ['', 'b-2', 'a-3', 'd-8'], // 4 events
  ['', 'b-0', 'b-1', 'b-2', 'a-1', 'b-4', 'a-4', 'b-6', 'c-2', 'b-9', 'c-3'] // 11 events
]

describe('Create Hashgraph Example', () => {
  it('should create hashgraph example with 4 events', async () => {
    const hashgraph = await createHashgraphExample([
      ['', 'b-0'],
      ['', 'a-1']
    ])

    const numberOfEvents = Object.keys(hashgraph.eventDataTable).length
    expect(numberOfEvents).to.equal(4)

    const numberOfMembers = Object.keys(hashgraph.memberDataTable).length
    expect(numberOfMembers).to.equal(2)
  })
  it('should create hashgraph example with N events', async () => {
    const hashgraph = await createHashgraphExample(exampleHashgraph2)

    const numberOfEvents = Object.keys(hashgraph.eventDataTable).length
    expect(numberOfEvents).to.equal(8 + 12 + 4 + 11)

    const numberOfMembers = Object.keys(hashgraph.memberDataTable).length
    expect(numberOfMembers).to.equal(4)
  })
})

export { exampleHashgraph2 }



function stringToBinary (value: string): number {
  const result: string[] = []
  for (let i = 0; i < value.length; i++) {
    result.push(value.charCodeAt(i).toString(2))
  }
  return parseInt(result.join(), 10)
}

export {
  stringToBinary
}

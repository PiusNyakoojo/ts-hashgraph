


import { Hashgraph, VoteTable } from '../../../../data-structures/hashgraph'


import { voteForEvent } from '../vote-for-event'

import { middleBitOfSignature } from '../../../../../../miscellaneous/bit-operations/algorithms/middle-bit-of-signature'

function determineFame (this: Hashgraph): void {
  const roundStartIndex = this.latestRoundReceived
  const roundEndIndex = this.latestRoundCreated

  const voteTable: VoteTable = {}

  for (let i = roundStartIndex; i < roundEndIndex; i++) {
    if (!this.eventRoundCreatedTable[i]) { continue }

    const roundWitnessList = Object.keys(this.eventRoundCreatedTable[i].witnessEventTable).map((eventUID) => this.eventDataTable[eventUID].event)

    const numActiveMembers = this.getMemberList(i).length
    // Filter out the rounds where all witnesses have already been calculated as famous or not famous
    let isFameDecidedForAll: boolean = numActiveMembers === roundWitnessList.length
    for (const eventX of roundWitnessList) {
      if (!this.eventDataTable[eventX.uid].isFameDecided) {
        isFameDecidedForAll = false
        break
      }
    }

    if (isFameDecidedForAll) {
      this.latestRoundReceived = i
      continue
    }

    for (let j = i + 1; j <= roundEndIndex; j++) {
      if (!this.eventRoundCreatedTable[j]) { continue }

      const laterRoundWitnessList = Object.keys(this.eventRoundCreatedTable[j].witnessEventTable).map((eventUID) => this.eventDataTable[eventUID].event)

      for (const eventX of roundWitnessList) {
        if (this.eventDataTable[eventX.uid].isFameDecided) {
          continue
        }

        if (!voteTable[eventX.uid]) {
          voteTable[eventX.uid] = {
            numOfUniqueMemberVoteYes: 0, numOfUniqueMemberVoteNo: 0, numOfUniqueMemberVoters: 0, voteRecord: {}, activeMemberTable: this.eventRoundCreatedTable[i].totalMemberTable
          }
        }

        for (const eventY of laterRoundWitnessList) {
          if (this.eventDataTable[eventX.uid].isFameDecided) {
            break
          }

          const roundDiff = this.eventDataTable[eventY.uid].roundCreated - this.eventDataTable[eventX.uid].roundCreated
          const stronglySeenWitnesses = this.stronglySeenRoundWitnesses(eventY, this.eventDataTable[eventY.uid].roundCreated - 1)

          let numVoteYes = voteTable[eventX.uid].numOfUniqueMemberVoteYes
          let numVoteNo = voteTable[eventX.uid].numOfUniqueMemberVoteNo
          for (const witness of stronglySeenWitnesses) {
            const canSeeEvent = this.canSeeEvent(witness, eventX)
            numVoteYes += canSeeEvent ? 1 : 0
            numVoteNo += !canSeeEvent ? 1 : 0
          }

          const majorityVote = numVoteYes >= numVoteNo
          const numMajorityVoters = majorityVote ? numVoteYes : numVoteNo

          if (!voteTable[eventX.uid].voteRecord[eventY.creatorUID]) {
            voteTable[eventX.uid].voteRecord[eventY.creatorUID] = { hasAnEventVotedYes: false, hasAnEventVotedNo: false, eventTable: {} }
          }

          if (!voteTable[eventX.uid].voteRecord[eventY.creatorUID].eventTable[eventY.uid]) {
            voteTable[eventX.uid].voteRecord[eventY.creatorUID].eventTable[eventY.uid] = { vote: false, hasVoted: false }
          }

          // This is the case for the first round in the election.
          if (roundDiff === 1) {
            // y.vote <- can y see x?
            const vote = this.canSeeEvent(eventY, eventX)
            voteForEvent(voteTable, eventX, eventY, vote)
          } else {
            // This is the case of a normal round.
            if (roundDiff % this.cFrequencyOfCoinRounds > 0) {
              if (numMajorityVoters >= Math.ceil((2 / 3) * numActiveMembers)) {
                this.decideForEvent(eventX, majorityVote)
                voteForEvent(voteTable, eventX, eventY, majorityVote) // y.vote <- v
                break
              } else {
                voteForEvent(voteTable, eventX, eventY, majorityVote) // y.vote <- v
              }

            // This is the case of a coin round.
            } else {
              if (numMajorityVoters >= Math.ceil((2 / 3) * numActiveMembers)) {
                voteForEvent(voteTable, eventX, eventY, majorityVote) // y.vote <- v
              } else {
                // y.vote <- middle bit of y signature
                voteForEvent(voteTable, eventX, eventY, middleBitOfSignature(eventY.signature) === 1 ? true : false)
              }
            }
          }
        }
      }
    }
  }
}

export {
  determineFame
}


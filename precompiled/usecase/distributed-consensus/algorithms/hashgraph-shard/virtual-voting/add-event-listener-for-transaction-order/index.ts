
import { HashgraphShard } from '../../../../data-structures/hashgraph-shard'

import * as uuid from 'uuid'

const transactionOrderEventId = 'transaction_order'
type transactionOrderEventListenerFunctionType = (event: { previousState: any, transactionList: any[], isConsensusOrder: boolean }) => Promise<void> | void

function addEventListenerForTransactionOrder (this: HashgraphShard, eventListenerFunction: transactionOrderEventListenerFunctionType): string {
  const eventListenerId = uuid.v4()

  if (!this.eventListenerTable[transactionOrderEventId]) {
    this.eventListenerTable[transactionOrderEventId] = {}
  }

  this.eventListenerTable[transactionOrderEventId][eventListenerId] = eventListenerFunction

  return eventListenerId
}

export {
  addEventListenerForTransactionOrder,
  transactionOrderEventListenerFunctionType,
  transactionOrderEventId
}


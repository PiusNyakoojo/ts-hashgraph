


import { Hashgraph } from '../../../../data-structures/hashgraph'
// import { HashgraphMember } from '../../../../data-structures/hashgraph-member'
import { HashgraphEvent } from '../../../../data-structures/hashgraph-event'


function decideForEvent (this: Hashgraph, candidateEvent: HashgraphEvent, decision: boolean): void {
    this.eventDataTable[candidateEvent.uid].isFamous = decision
    this.eventDataTable[candidateEvent.uid].isFameDecided = true

    if (decision) {
      this.eventRoundCreatedTable[this.eventDataTable[candidateEvent.uid].roundCreated].famousEventTable[candidateEvent.uid] = true
    } else {
      this.eventRoundCreatedTable[this.eventDataTable[candidateEvent.uid].roundCreated].notFamousEventTable[candidateEvent.uid] = true
    }
  }

export {
  decideForEvent
}



// import { HashgraphShard } from '../../../data-structures/hashgraph-shard'

// function functionName (this: HashgraphShard) {
//     //
// }

// async determineTransactionOrder (shard: HashgraphShard): Promise<void> {

//     // Determine Fame and Order of Events
//     this.hashgraphUsecase.determineFame(shard.hashgraph)
//     const orderedEventList = this.hashgraphUsecase.determineOrder(shard.hashgraph, shard.latestRoundReceived - 1)

//     const roundTransactionTable: { [roundReceived: number]: Array<{ timestamp: string, payload: any }>} = {}

//     // Handle shard-related transactions
//     for (let i = 0; i < orderedEventList.length; i++) {
//       const event = orderedEventList[i]

//       if (!roundTransactionTable[event.roundReceived]) {
//         roundTransactionTable[event.roundReceived] = []
//       }

//       for (let j = 0; j < event.payload.length; j++) {
//         const transaction = event.payload[j]

//         let messageType: string = ''
//         if (typeof transaction === 'object' && transaction !== null) {
//           messageType = transaction[HASHGRAPH_SHARD_MESSAGE_TYPE]
//         }

//         // Add the non-shard-related transactions to roundTransactionTable
//         // for the user to process.
//         if (!messageType) {
//           roundTransactionTable[event.roundReceived].push({ timestamp: event.consensusTimestamp, payload: transaction })
//           continue
//         }

//         switch (messageType) {
//           case HASHGRAPH_SHARD_ADD_MEMBER_NOMINATION:
//             await this.onAddMemberNomination(shard, transaction.candidateMember, event.roundCreated)
//             break
//           case HASHGRAPH_SHARD_REMOVE_MEMBER_NOMINATION:
//             await this.onRemoveMemberNomination(shard, transaction.candidateMember, event.roundCreated)
//             break
//           case HASHGRAPH_SHARD_ADD_MEMBER_VOTE:
//             await this.onAddMemberVote(shard, transaction.candidateMember, transaction.votingMember, transaction.vote, event.roundCreated)
//             break
//           case HASHGRAPH_SHARD_REMOVE_MEMBER_VOTE:
//             await this.onRemoveMemberVote(shard, transaction.candidateMember, transaction.votingMember, transaction.vote, event.roundCreated)
//             break
//           default:
//             break
//         }
//       }
//     }

//     if (!shard.onTransactionOrderCallback) { return }

//     // Apply transactions to the state for each round received
//     const roundTransactionList = Object.keys(roundTransactionTable).map((round: string) => parseInt(round, 10)).sort((a, b) => a - b)

//     for (const roundReceived of roundTransactionList) {
//       const previousState = shard.stateTable[roundReceived - 1]  || {}
//       const previousStateCopy = JSON.parse(JSON.stringify(previousState))
//       const transactionList = roundTransactionTable[roundReceived]

//       await shard.onTransactionOrderCallback!(previousStateCopy, transactionList, true)

//       shard.stateTable[roundReceived] = JSON.parse(JSON.stringify(previousStateCopy))
//     }

//     const newLatestRoundReceived = roundTransactionList[roundTransactionList.length - 2]

//     if (newLatestRoundReceived > shard.latestRoundReceived) {
//       shard.latestRoundReceived = newLatestRoundReceived
//     }
//   }

// export {
//     functionName
// }

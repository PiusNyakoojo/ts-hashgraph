
import { HashgraphShard } from '../../../../data-structures/hashgraph-shard'
import { HashgraphMember } from '../../../../data-structures/hashgraph-member'

async function addMember (this: HashgraphShard, {
  peerID,
  isSelfPeer,
  isConnectedPeer,
  candidateMember,
  skipVoting,
  round
} : {
  peerID?: string,
  isSelfPeer?: boolean,
  isConnectedPeer?: boolean,
  candidateMember?: HashgraphMember,
  skipVoting?: boolean,
  round?: number
}): Promise<string> {
    peerID = peerID || (candidateMember ? candidateMember.peerID : '')
    let memberUid = this.hashgraph.memberDataToUidMap.peerIdMap[peerID || ''] || ''
    let publicKey = this.hashgraph.memberDataTable[memberUid] ? this.hashgraph.memberDataTable[memberUid].member.publicKey || '' : ''

    if (candidateMember) {
      publicKey = candidateMember.publicKey || ''

      if (!publicKey) {
        console.warn(`CandidateMember must have 'publicKey' property when calling addMember()`)
        return ''
      }
    }

    const isAlreadyAdded = this.hashgraph.memberDataToUidMap.publicKeyMap[publicKey] ? true : false
    if (isAlreadyAdded) { return publicKey }

    const selfMember = this.hashgraph.getMember(this.selfPeerID)

    let numOfConnectedPeers = 0
    if (selfMember) {
      numOfConnectedPeers = Object.keys(this.hashgraph.memberDataTable[selfMember.uid].member.peerConnectionMap!).length
    }

    // Add self member without voting
    if (isSelfPeer && !this.selfPeerID) {
      const member = new HashgraphMember({
        peerID
      })
      peerID = peerID ? peerID : member.publicKey
      publicKey = member.publicKey || ''
      this.selfPeerID = peerID || ''
      this.hashgraph.addMember(member, round)

      // Create an initial event when adding self as a member
      const newEvent = await member.createEvent([])
      await this.hashgraph.addEvent(newEvent)

      // if (shard.onMemberUpdateCallback) {
      //   shard.onMemberUpdateCallback({ publicKey, peerID }, MEMBER_UPDATE_TYPE_ADDED)
      // }
      // delete this.addMemberCandidateTable[publicKey]

      // if (shard.onReceiveGossipCallback) {
      //   shard.onReceiveGossipCallback([newEvent])
      // }

    // Add member without voting when skipVoting is enabled or is second member
    } else if (skipVoting || numOfConnectedPeers < 1) {
      if (!publicKey) { return '' }
      const member = new HashgraphMember({
        publicKey,
        peerID
      })
      peerID = peerID ? peerID : member.publicKey
      this.hashgraph.addMember(member, round)

      if (selfMember && isConnectedPeer) {
        this.hashgraph.memberDataTable[selfMember.uid].member.peerConnectionMap![publicKey] = true
      }

      // if (shard.onMemberUpdateCallback) {
      //   shard.onMemberUpdateCallback(candidateMember!, MEMBER_UPDATE_TYPE_ADDED)
      // }

    // Add member through community voting to approve new member
    } else if (publicKey && this.selfPeerID) {
      // if (!candidateMember!.peerConnectionMap) { candidateMember!.peerConnectionMap = {} }
      // if (isConnectedPeer) { candidateMember!.peerConnectionMap![shard.peerIDPublicKeyMap[shard.selfPeerID]] = true }

      // this.sendTransaction({
      //   [HASHGRAPH_SHARD_MESSAGE_TYPE]: HASHGRAPH_SHARD_ADD_MEMBER_NOMINATION,
      //   candidateMember
      // })
    }

    return publicKey
  }

export {
  addMember
}

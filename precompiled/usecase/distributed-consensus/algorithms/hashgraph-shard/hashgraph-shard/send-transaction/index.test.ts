
import { expect } from 'chai'

import { HashgraphShard } from '../../../../data-structures/hashgraph-shard'

describe('Hashgraph Shard - Send Transaction', () => {
  it('should queue transaction to be sent', async () => {
    const shard = new HashgraphShard()

    expect(shard.transactionQueue.length).to.equal(0)

    shard.sendTransaction('Hello World')

    expect(shard.transactionQueue.length).to.equal(1)
  })
})


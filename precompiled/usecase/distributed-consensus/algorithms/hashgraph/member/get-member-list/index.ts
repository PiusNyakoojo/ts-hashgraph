
import { Hashgraph } from '../../../../data-structures/hashgraph'
import { HashgraphMember } from '../../../../data-structures/hashgraph-member'


function getMemberList (this: Hashgraph, round?: number): HashgraphMember[] {

  round = round || this.latestRoundCreated

  if (!round || !this.eventRoundCreatedTable[round]) {
    return []
  }

  return Object.keys(this.eventRoundCreatedTable[round].totalMemberTable).map((memberUID: string) => this.memberDataTable[memberUID].member)
}

export {
  getMemberList
}

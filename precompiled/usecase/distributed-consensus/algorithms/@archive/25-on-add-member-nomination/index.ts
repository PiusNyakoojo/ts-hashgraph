
// import { HashgraphShard } from '../../../data-structures/hashgraph-shard'

// function functionName (this: HashgraphShard) {
//     //
// }

// async onAddMemberNomination (shard: HashgraphShard, candidateMember: HashgraphShardMember, roundNominated: number): Promise<void> {

//     // Ensure candidate isn't already active.
//     if (this.hashgraphUsecase.isActiveMember(shard.hashgraph, candidateMember.publicKey)) { return }

//     // Ensure member hasn't already been nominated.
//     if (shard.addMemberCandidateTable[candidateMember.publicKey]) { return }

//     let minimumVotePercent: number = shard.defaultMinimumVotePercent
//     if (shard.setVotePercentToAddMemberCallback) {
//       minimumVotePercent = await shard.setVotePercentToAddMemberCallback(candidateMember)
//     }

//     // Add the member as a candidate
//     shard.addMemberCandidateTable[candidateMember.publicKey] = {
//       candidate: candidateMember, roundNominated, voteTable: {}, minimumVotePercent
//     }

//     const selfMemberPublicKey: string = this.getMemberPublicKey(shard, shard.selfPeerID)

//     if (!selfMemberPublicKey) { return }

//     let selfMemberVote: boolean = true
//     if (shard.onAddMemberCallback) {
//       selfMemberVote = await shard.onAddMemberCallback(candidateMember)
//     }

//     // Send a vote
//     this.sendTransaction(shard, {
//       [HASHGRAPH_SHARD_MESSAGE_TYPE]: HASHGRAPH_SHARD_ADD_MEMBER_VOTE,
//       candidateMember, votingMember: { publicKey: selfMemberPublicKey }, vote: selfMemberVote
//     })
//   }

// export {
//     functionName
// }
